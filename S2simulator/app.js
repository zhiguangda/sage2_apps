// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2016


// Variable appData contains the equivalent of instructions.json

// Slideshow app
// var appData = {
// 	id: "app_0",
// 	application: "photos",
// 	x: 0, y: 0,
// 	width: 600, height: 600,
// 	animation: true,
// 	resrc: [
// 		"photo_scrapbooks.js"
// 	],
// 	state: {
// 		imageSet: 0,
// 		counter: 1
// 	},
// 	url: "apps/photos",
// 	date: new Date()
// };


// Scale-div app
// var appData = {
// 	id: "app_0",
// 	application: "scale_div",
// 	x: 0, y: 0,
// 	width: 800, height: 400,
// 	resrc: [],
// 	state: {},
// 	url: "apps/scale_div",
// 	date: new Date()
// };

// googlemaps
var appData = {
	id: "app_0",
	application: "googlemaps",
	x: 0, y: 0,
	width: 800, height: 400,
	resrc: [
		"googlemapsInit.js",
		"https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=weather&callback=googleMapsForSAGE2Init"
	],
	state: {
		mapType:   "hybrid",
		zoomLevel: 8,
		center:    {lat: 41.850033, lng: -87.6500523},
		layer:     {w: false, t: false}
	},
	url: "apps/googlemaps",
	date: new Date()
};

// Car demo
// var appData = {
// 	id: "app_0",
// 	application: "car_threejs",
// 	x: 0, y: 0,
// 	width: 700, height: 500,
// 	animation: true,
// 	resrc: [
// 		"/lib/three.min.js",
// 		"scripts/OrbitControls.js",
// 		"scripts/ctm/lzma.js",
// 		"scripts/ctm/ctm.js",
// 		"scripts/ctm/CTMLoader.js"
// 	],
// 	state: {
// 		mapType:   "hybrid",
// 		zoomLevel: 8,
// 		center:    {lat: 41.850033, lng: -87.6500523},
// 		layer:     {w: false, t: false}
// 	},
// 	url: "apps/car_threejs",
// 	date: new Date()
// };

// Clock app
// var appData = {
// 	id: "app_0",
// 	application: "clock_svg",
// 	x: 0, y: 0,
// 	width: 600, height: 600,
// 	animation: true,
// 	resrc: [],
// 	state: {
// 		mode: 1
// 	},
// 	url: "apps/clock_svg",
// 	date: new Date()
// };
