// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2015-16

var clientID = 0;
var isMaster = true;

var applications   = {};
var dependencies   = {};
var controlObjects = {};

var userPointer;
var ptrX = 0, ptrY = 0;

var ui = {};
ui.offsetX = 0;
ui.offsetY = 0;
ui.json_cfg = {};
ui.json_cfg.ui = {};
ui.json_cfg.ui.titleTextSize = 16;
ui.json_cfg.resolution = {};
ui.json_cfg.resolution.width  = window.innerWidth;
ui.json_cfg.resolution.height = window.innerHeight;

var minDim = Math.min(ui.json_cfg.resolution.width, ui.json_cfg.resolution.height);
var maxDim = Math.max(ui.json_cfg.resolution.width, ui.json_cfg.resolution.height);

ui.json_cfg.ui.pointerSize = Math.round(0.08 * minDim);


var pointerWidth   = ui.json_cfg.ui.pointerSize * 3;
var pointerHeight  = ui.json_cfg.ui.pointerSize;
var widgetControlSize = ui.json_cfg.ui.widgetControlSize;
var pointerOffsetX    = Math.round(0.27917 * pointerHeight);
var pointerOffsetY    = Math.round(0.24614 * pointerHeight);

function loadApplication(data, callback) {
	var init = {
		id: data.id,
		x: data.left,
		y: data.top,
		width: data.width,
		height: data.height,
		resrc: data.url,
		state: data.state,
		date: data.date
	};

	// load new app
	if (window[data.application] === undefined) {
		var js = document.createElement("script");
		js.addEventListener('error', function(event) {
			console.log("Error loading script: " + data.application + ".js");
		}, false);
		js.addEventListener('load', function(event) {
			var newapp = new window[data.application]();
			newapp.init(init);
			newapp.refresh(data.date);
			applications[data.id]   = newapp;
			controlObjects[data.id] = newapp;
			callback();
		}, false);
		js.type  = "text/javascript";
		js.async = false;
		js.src = data.url + "/" + data.application + ".js";
		document.head.appendChild(js);
	} else {
		// load existing app
		var app = new window[data.application]();
		app.init(init);
		app.refresh(data.date);

		applications[data.id]   = app;
		controlObjects[data.id] = app;

		if (data.application === "movie_player") {
			// setTimeout(function() { wsio.emit('requestVideoFrame', {id: data.id}); }, 500);
		}
		callback();
	}
}

function getTransform(elem) {
	var transform = elem.style.transform;
	var translate = {x: 0, y: 0};
	var scale = {x: 1, y: 1};
	if (transform) {
		var tIdx = transform.indexOf("translate");
		if (tIdx >= 0) {
			var tStr = transform.substring(tIdx + 10, transform.length);
			tStr = tStr.substring(0, tStr.indexOf(")"));
			var tValue = tStr.split(",");
			translate.x = parseFloat(tValue[0]);
			translate.y = parseFloat(tValue[1]);
		}
		var sIdx = transform.indexOf("scale");
		if (sIdx >= 0) {
			var sStr = transform.substring(sIdx + 6, transform.length);
			sStr = sStr.substring(0, sStr.indexOf(")"));
			var sValue = sStr.split(",");
			scale.x = parseFloat(sValue[0]);
			scale.y = parseFloat(sValue[1]);
		}
	}
	return {translate: translate, scale: scale};
}

function createApp(data, callback) {
	var windowItem = document.createElement("div");
	windowItem.id  = data.id;
	windowItem.className    = "windowItem";
	windowItem.style.left   = "0px";
	windowItem.style.top    = "0px";

	windowItem.style.width  = data.width.toString()  + "px";
	windowItem.style.height = data.height.toString() + "px";

	// windowItem.style.webkitTransform = translate;
	// windowItem.style.mozTransform    = translate;
	// windowItem.style.transform       = translate;
	windowItem.style.overflow = "hidden";
	windowItem.style.zIndex   = "1";
	windowItem.style.boxShadow = "none";
	var root = document.getElementById('root');
	root.appendChild(windowItem);

	// load all dependencies
	if (data.resrc === undefined || data.resrc === null || data.resrc.length === 0) {
		loadApplication(data, callback);
	} else {
		var loadResource = function(idx) {
			if (dependencies[data.resrc[idx]] !== undefined) {
				if ((idx + 1) < data.resrc.length) {
					loadResource(idx + 1);
				} else {
					console.log("all resources loaded", data.id);
					loadApplication(data, callback);
				}
				return;
			}

			dependencies[data.resrc[idx]] = false;

			var js = document.createElement("script");
			js.addEventListener('error', function(event) {
				console.log("Error loading script: " + data.resrc[idx]);
			}, false);

			js.addEventListener('load', function(event) {
				dependencies[data.resrc[idx]] = true;

				if ((idx + 1) < data.resrc.length) {
					loadResource(idx + 1);
				} else {
					console.log("all resources loaded", data.id);
					loadApplication(data, callback);
				}
			});
			js.type  = "text/javascript";
			js.async = false;
			if (data.resrc[idx].indexOf("http://")  === 0 ||
				data.resrc[idx].indexOf("https://") === 0 ||
				data.resrc[idx].indexOf("/") === 0) {
				js.src = data.resrc[idx];
			} else {
				js.src = data.url + "/" + data.resrc[idx];
			}
			document.head.appendChild(js);
		};
		// Start loading the first resource
		loadResource(0);
	}
}

function pointerPress(event) {
	var app = applications[appData.id];
	if ( (ptrX < app.sage2_width) && (ptrY < app.sage2_height) ) {
		app.SAGE2Event('pointerPress', {x: ptrX, y: ptrY}, "127.0.0.1:12345",
			{button: "left"}, new Date());
	}
	return event.preventDefault();
}
function pointerRelease(event) {
	var app = applications[appData.id];
	if ( (ptrX < app.sage2_width) && (ptrY < app.sage2_height) ) {
		app.SAGE2Event('pointerRelease', {x: ptrX, y: ptrY}, "127.0.0.1:12345",
			{button: "left"}, new Date());
	}
	event.preventDefault();
}
function pointerMove(event) {
	var x   = event.clientX;
	var y   = event.clientY;
	var app = applications[appData.id];
	if ( app && (x < app.sage2_width) && (y < app.sage2_height) ) {
		updateSagePointerPosition({
			left: x, top: y
		});
		app.SAGE2Event('pointerMove', {x: ptrX, y: ptrY}, "127.0.0.1:12345",
			{}, new Date());
	}	
	event.preventDefault();
}

function pointerScroll(event) {
	var app = applications[appData.id];
	if ( (ptrX < app.sage2_width) && (ptrY < app.sage2_height) ) {
		app.SAGE2Event('pointerScroll', {x: ptrX, y: ptrY}, "127.0.0.1:12345",
			{wheelDelta: event.deltaY}, new Date());
	}
	event.preventDefault();
}

function keyDown(event) {
	var app = applications[appData.id];
	if ( (ptrX < app.sage2_width) && (ptrY < app.sage2_height) ) {
		var code = parseInt(event.keyCode, 10);
		app.SAGE2Event('specialKey', {x: ptrX, y: ptrY}, "127.0.0.1:12345",
			{code: code, state: "down"},
			new Date());
	}
	// event.preventDefault();
}
function keyUp(event) {
	var app = applications[appData.id];
	if ( (ptrX < app.sage2_width) && (ptrY < app.sage2_height) ) {
		var code = parseInt(event.keyCode, 10);
		app.SAGE2Event('specialKey', {x: ptrX, y: ptrY}, "127.0.0.1:12345",
			{code: code, state: "up"},
			new Date());
	}
	// event.preventDefault();
}
function keyPress(event) {
	var app = applications[appData.id];
	if ( (ptrX < app.sage2_width) && (ptrY < app.sage2_height) ) {
		var code = parseInt(event.keyCode, 10);
		app.SAGE2Event('keyboard', {x: ptrX, y: ptrY}, "127.0.0.1:12345",
			{code: code, character: String.fromCharCode(code), state: "up"},
			new Date());
	}
	event.preventDefault();
}

function updateSagePointerPosition(pointer_data) {
	var pointerElem = userPointer.div;

	ptrX = pointer_data.left;
	ptrY = pointer_data.top;

	var translate;
	translate = "translate(" + pointer_data.left + "px," + pointer_data.top + "px)";

	pointerElem.style.webkitTransform = translate;
	pointerElem.style.mozTransform    = translate;
	pointerElem.style.transform       = translate;
}


function createSagePointer(pointer_data) {
	var pointerElem = document.createElement('div');
	pointerElem.id  = pointer_data.id;
	pointerElem.className  = "pointerItem";
	pointerElem.style.zIndex = 10000;

	pointerElem.style.left = (-pointerOffsetX - ui.offsetX).toString() + "px";
	pointerElem.style.top  = (-pointerOffsetY - ui.offsetY).toString()  + "px";
	var root = document.getElementById('root');
	root.appendChild(pointerElem);

	var ptr = new Pointer();
	ptr.init(pointerElem.id, pointer_data.label, pointer_data.color, pointerWidth, pointerHeight);

	if (pointer_data.visible) {
		pointerElem.style.display = "block";
		ptr.isShown = true;
	} else {
		pointerElem.style.display = "none";
		ptr.isShown = false;
	}

	return ptr;
}


function SAGE2_init() {
	console.log("SAGE2> simulator on");

    var appRatio = appData.width / appData.height;
    var app;

	window.onresize = function() {
		var ww = document.documentElement.clientWidth;
		var hh = document.documentElement.clientWidth / appRatio;
		var browserRatio = ww / hh;
		var d = document.getElementById(appData.id);
		d.style.width  = ww.toString() + "px";
		d.style.height = hh.toString() + "px";
		if (app) {
			app.element.style.width  = ww.toString() + "px";
			app.element.style.height = hh.toString() + "px";
			app.sage2_x      = 0;
			app.sage2_y      = 0;
			app.sage2_width  = ww;
			app.sage2_height = hh;
			if (app.resize) {
				app.resize(new Date());
			}
		}
	};

	// Create a pointer
	userPointer = createSagePointer({
		id: "127.0.0.1:12345",
		label: "user",
		color: "#FF0000",
		visible: true
	});
	// App mode
	userPointer.changeMode(1);

	document.addEventListener('mousedown',  pointerPress,    false);
	document.addEventListener('mouseup',    pointerRelease,  false);
	document.addEventListener('mousemove',  pointerMove,     false);
	document.addEventListener('wheel',      pointerScroll,   false);
	document.addEventListener('keydown',    keyDown,  false);
	document.addEventListener('keyup',      keyUp,    false);
	document.addEventListener('keypress',   keyPress, false);

	createApp(appData, function() {
		app = applications[appData.id];
		window.onresize();

		// Seems slow:
		// var appID = document.getElementById(appData.id);
		// appID.addEventListener('mousedown',  pointerPress,    false);
		// appID.addEventListener('mouseup',    pointerRelease,  false);
		// appID.addEventListener('mousemove',  pointerMove,     false);
		// appID.addEventListener('wheel',      pointerScroll,   false);
	});

	if (appData.animation === true) {
		setInterval(function() {
			if (app) {
				app.refresh(new Date());
			}
		}, 50);
	}

}
