/*
 * Bulk movieplayer.  Plays uncompressed movies as encoded by "img2bmv",
 * using bpio threaded I/O library and OpenGL texturing.
 *
 * Stuart Levy, NCSA, University of Illinois Urbana-Champaign, June, 2004.
 */

/*
 * $Log: bplay-noglut.c,v $
 * Revision 1.1  2006/07/04 00:54:52  slevy
 * Snapshot of bplay with Luc Renambot's SAGE additions,
 * merged with bplay updates by slevy.
 *
 * Revision 1.50  2006/01/15 06:32:32  slevy
 * Restart global clock (properly) when movie cycles back to beginning.
 *
 * Revision 1.49  2006/01/14 05:31:30  slevy
 * Include accumulated time drift in on-screen display too.
 *
 * Revision 1.48  2006/01/14 05:17:14  slevy
 * More complete command-line usage.
 * Include version number in Usage message.
 *
 * Enable global timing by default (let's see how well this does!).
 * Use -T 0 to suppress this.
 *
 * Revision 1.47  2006/01/14 05:05:55  slevy
 * Add log history.
 * Add version id string.
 *
 * ----------------------------
 * revision 1.46
 * date: 2006/01/14 04:59:54;  author: slevy;  state: Exp;  lines: +50 -40
 * Maybe -R works now?
 * Report msaccum (timeshift relative to global clock) as well as ms-since-last-frame.
 * Add -T option and 'T' key command to enable/disable global timing.
 * ----------------------------
 * revision 1.45
 * date: 2006/01/14 04:26:50;  author: slevy;  state: Exp;  lines: +165 -31
 * Add global-clock sync.  Maybe.
 * Add -R "command involving %s" -- enables global-clock sync,
 * and starts the given shell command 300 ms before playback starts.
 * Any "%s" is replaced with the stem-name for the movie file,
 * with any .bmv suffix stripped.  E.g.  -R "play %s.wav".
 * Use -P option to adjust startup offset, e.g. -P -300  or -300P interactively.
 * Print (to stderr) current keyboard-adjustable settings with "h"/"?" key.
 * ----------------------------
 * revision 1.44
 * date: 2005/12/03 02:27:39;  author: slevy;  state: Exp;  lines: +37 -12
 * Make legend show up in stereo too, and include movie filename.
 * ----------------------------
 * revision 1.43
 * date: 2005/12/03 01:36:04;  author: slevy;  state: Exp;  lines: +5 -4
 * Use 2 threads by default now -- seems to work on the machines we have.
 * 3 buffers per ring are plenty.
 * 'v' key toggles between verbose = 2 & 0.
 * ----------------------------
 * revision 1.42
 * date: 2005/11/30 07:36:17;  author: slevy;  state: Exp;  lines: +23 -19
 * Make keyboard work better: left-arrow and "<" go backwards, right-arrow and ">" go forwards.
 * Try harder to avoid hanging at beginning- or end-of-movie.
 * Display left/right relshift on screen.
 * Move legend down a bit.  (Hack.)
 * ----------------------------
 * revision 1.41
 * date: 2005/11/15 06:58:25;  author: slevy;  state: Exp;  lines: +23 -5
 * Autodetect quadbuffered-capable displays -- do quadbuffered stereo if
 * possible, crosseyed otherwise, unless user specifies -q/-m/-c.
 * ----------------------------
 * revision 1.40
 * date: 2005/11/14 06:26:13;  author: slevy;  state: Exp;  lines: +7 -7
 * In draweye(), use xoff as intended: as the starting offset for that eye's image.
 * So we add xright to it in crosseyed mode, and nothing to it in quadbuffered mode.
 * ----------------------------
 * revision 1.39
 * date: 2005/11/14 06:13:39;  author: slevy;  state: Exp;  lines: +5 -5
 * Half-fix for quadbuffered stereo.  Works as long as image size = screen size.
 * ----------------------------
 * revision 1.38
 * date: 2005/11/10 09:13:32;  author: slevy;  state: Exp;  lines: +2 -0
 * Set texture edge behavior to GL_CLAMP_TO_EDGE.  Does this get rid of
 * glitchy artifacts?
 * ----------------------------
 * revision 1.37
 * date: 2005/10/27 05:28:24;  author: slevy;  state: Exp;  lines: +7 -4
 * Report window size in verbose >=2 mode.
 * Be more careful about visible().  With freeglut,
 * it appears we get some redundant visible() callbacks,
 * one before the window becomes visible, and two more once it is visible.
 * ----------------------------
 * revision 1.36
 * date: 2005/10/19 15:36:22;  author: slevy;  state: Exp;  lines: +1 -0
 * Make stdout line-buffered so "tail -f movie.log" works well.
 * ----------------------------
 * revision 1.35
 * date: 2005/10/19 02:15:54;  author: slevy;  state: Exp;  lines: +5 -2
 * Hide cursor.
 * Call our graphics window "bplay", not "eep", for Pete's sake.
 * ----------------------------
 * revision 1.34
 * date: 2005/10/15 00:14:52;  author: slevy;  state: Exp;  lines: +1 -0
 * flush log messages after each print.
 * ----------------------------
 * revision 1.33
 * date: 2005/10/14 23:04:51;  author: slevy;  state: Exp;  lines: +2 -1
 * Print Usage and quit on unrecognized options.
 * ----------------------------
 * revision 1.32
 * date: 2004/11/18 07:35:24;  author: slevy;  state: Exp;  lines: +122 -88
 * Implement quadbuffered stereo (-q option), I hope.
 * ----------------------------
 * revision 1.31
 * date: 2004/11/09 17:43:44;  author: slevy;  state: Exp;  lines: +7 -2
 * Allow setting msfudge with -M option.
 * ----------------------------
 * revision 1.30
 * date: 2004/10/29 16:59:40;  author: slevy;  state: Exp;  lines: +7 -5
 * Accommodate prefrate=-1 (bug in img2bmv) to mean "no preferred rate set".
 * Default to using big (8MB) reads for *much* better performance!
 * ----------------------------
 * revision 1.29
 * date: 2004/10/29 02:58:41;  author: slevy;  state: Exp;  lines: +2 -2
 * Switch to version 3 bpmovie magic number.  This version has the same fields,
 * but reordered so that the 'long long' field is aligned on an 8-byte boundary,
 * so sane 32- and 64-bit C compilers agree on layout.
 * ----------------------------
 * revision 1.28
 * date: 2004/07/12 16:05:12;  author: slevy;  state: Exp;  lines: +21 -9
 * Be careful about where 'g' is willing to seek to.  99999g should
 * go to last frame of movie (but does it??).
 * 
 * Add 't' key, which directly sets frame-time in milliseconds.
 * 
 * Now that bpio.c uses O_DIRECT, retune default read-size and reader-count:
 * use just 4 filler threads and larger, 1MB reads.
 * ----------------------------
 * revision 1.27
 * date: 2004/07/02 00:59:26;  author: slevy;  state: Exp;  lines: +2 -2
 * Another debug msg.
 * ----------------------------
 * revision 1.26
 * date: 2004/07/01 07:37:47;  author: slevy;  state: Exp;  lines: +80 -24
 * Make keystrokes more consistent.  Ctrl-left and Ctrl-right now shift images relative
 * to each other.  "<" and ">" work.  "+" and "-" don't step; "+" is ignored and "-" is
 * a numeric prefix.
 * 
 * Add relative shifting of left vs right-half images.
 * Accept -L/-R options, with optional "!" numeric suffix to override
 * movies' own preferences.
 * 
 * Discard -C option, we can live without it.
 * ----------------------------
 * revision 1.25
 * date: 2004/06/30 15:55:28;  author: slevy;  state: Exp;  lines: +33 -9
 * "s" key alone resets to default speed.
 * "P" key, "-P" option set millisecs preload time when switching movies.
 * ----------------------------
 * revision 1.24
 * date: 2004/06/30 03:16:26;  author: slevy;  state: Exp;  lines: +1 -1
 * Fix -f option.
 * ----------------------------
 * revision 1.23
 * date: 2004/06/30 01:32:40;  author: slevy;  state: Exp;  lines: +20 -5
 * Up and Down keys change skip factors.
 * ----------------------------
 * revision 1.22
 * date: 2004/06/30 01:17:56;  author: slevy;  state: Exp;  lines: +15 -3
 * Movies can now specify their own preferred frame rates.
 * Use -F <fps> to override.
 * ----------------------------
 * revision 1.21
 * date: 2004/06/29 23:35:52;  author: slevy;  state: Exp;  lines: +67 -15
 * Add frame-skipping ('y' key), controlfile (not yet implemented),
 * make looping work across playlists.
 * ----------------------------
 * revision 1.20
 * date: 2004/06/29 15:21:30;  author: slevy;  state: Exp;  lines: +42 -11
 * Add -V option: video frame rate -- allows us to use glXSwapIntervalSGI()
 * to do better timing than we could ourselves.
 * ----------------------------
 * revision 1.19
 * date: 2004/06/26 20:12:08;  author: slevy;  state: Exp;  lines: +33 -25
 * Move frame-timing to *after* glutSwapBuffers(), for better stability.
 * If verbose>=4, neither draw nor glClear().
 * ----------------------------
 * revision 1.18
 * date: 2004/06/25 20:18:36;  author: slevy;  state: Exp;  lines: +20 -11
 * Measure time from after glutSwapBuffers(), not before.
 * If data is on an external file, use nframes to measure movie length
 * instead of letting bpopen() measure total file length.
 * ----------------------------
 * revision 1.17
 * date: 2004/06/25 16:21:46;  author: slevy;  state: Exp;  lines: +19 -7
 * Add bpsync(), bpb->busy flags, so we stall the pipeline (bpstop())
 * and wait for all threads to notice it.
 * Maybe finally starting to work, at least when playing forward...
 * ----------------------------
 * revision 1.16
 * date: 2004/06/25 06:16:22;  author: slevy;  state: Exp;  lines: +17 -13
 * In playlistadvance(), don't resumemovie if we were paused,
 * i.e. don't jump the gun.
 * More debug printfs, lint picking.
 * Only autostart the movie the *first* time the window becomes visible!
 * ----------------------------
 * revision 1.15
 * date: 2004/06/25 05:38:38;  author: slevy;  state: Exp;  lines: +6 -3
 * More debugging tweaks...
 * ----------------------------
 * revision 1.14
 * date: 2004/06/25 03:30:01;  author: slevy;  state: Exp;  lines: +8 -1
 * Allow compiling with free GLUT (as distributed with Fedora) using -DUSE_FREEGLUT=1.
 * ----------------------------
 * revision 1.13
 * date: 2004/06/24 07:46:31;  author: slevy;  state: Exp;  lines: +19 -12
 * More debugging messages.
 * NOFS env var -> run in window, not full screen.
 * Get image-resizing logic working sensibly.
 * ----------------------------
 * revision 1.12
 * date: 2004/06/23 22:57:25;  author: slevy;  state: Exp;  lines: +12 -9
 * Help with debugging...
 * ----------------------------
 * revision 1.11
 * date: 2004/06/23 22:37:49;  author: slevy;  state: Exp;  lines: +6 -5
 * Small fixes...
 * But something seems wrong with buffering now -- it's playing frames out of order
 * when using large numbers of threads?
 * ----------------------------
 * revision 1.10
 * date: 2004/06/23 22:13:26;  author: slevy;  state: Exp;  lines: +59 -21
 * Try to scale&translate image to fit screen.
 * ----------------------------
 * revision 1.9
 * date: 2004/06/23 21:27:12;  author: slevy;  state: Exp;  lines: +108 -29
 * Parse command line options -- frame rate, verbose mode, thread count, etc.
 * Convert to new movie format with explicit "BPF_STEREO" flag.
 * Add "port" flag for potential future use.
 * ----------------------------
 * revision 1.8
 * date: 2004/06/22 06:12:51;  author: slevy;  state: Exp;  lines: +1 -0
 * Gotcha.  Remember to reinitialize textures.
 * ----------------------------
 * revision 1.7
 * date: 2004/06/22 06:10:15;  author: slevy;  state: Exp;  lines: +89 -54
 * Make playlists begin to work, but not completely...
 * ----------------------------
 * revision 1.6
 * date: 2004/06/22 05:15:52;  author: slevy;  state: Exp;  lines: +76 -23
 * Try adding simple playlist behavior.
 * ----------------------------
 * revision 1.5
 * date: 2004/06/17 06:04:55;  author: slevy;  state: Exp;  lines: +0 -12
 * Move bpcurbuf() into bpio.c.
 * ----------------------------
 * revision 1.4
 * date: 2004/06/17 05:47:09;  author: slevy;  state: Exp;  lines: +28 -11
 * Fix discipline for toggling glutIdleFunc() -- resumemovie() only
 * starts the idler if we had been paused before.
 * Add debugging messages, written to stdout if verbose >= 2 or more if >= 3.
 * ----------------------------
 * revision 1.3
 * date: 2004/06/15 03:33:30;  author: slevy;  state: Exp;  lines: +26 -2
 * Add 'l'(oop) key, 'h'(elp) key.
 * ----------------------------
 * revision 1.2
 * date: 2004/06/14 20:49:07;  author: slevy;  state: Exp;  lines: +139 -9
 * Lots of changes.  Add frame rate control, forward/backward, misc keyboard commands:
 *   <NNN>s   aim for NNN frames/sec
 *   <NNN>g   go to NNN'th frame and pause (first = 0)
 *    .       one frame forward
 *    ,       one frame backward
 *    f       run forward
 *    b       run backward
 *    v       toggle verbose (on-screen framenumber & fps counter)
 *    p	   pause
 *   SPACE    toggle run/pause
 * ----------------------------
 * revision 1.1
 * date: 2004/06/12 01:08:31;  author: slevy;  state: Exp;
 * Bulk movie player.
 */

static char id[] = "$Id: bplay-noglut.c,v 1.1 2006/07/04 00:54:52 slevy Exp $";

#include "bpio.h"	/* Need this first since it #defines some large-file options */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <signal.h>
#include <errno.h>
#include <sys/time.h>
#include <getopt.h>

#include <unistd.h>

#include "bpmovie.h"

// headers for SAGE
#include "sail2.h"
sail *sageInf;

unsigned char *rgbBuffer = 0;


/*
struct txcodes {
    int internalfmt;
    int format;
    int type;
} txcode[] = {
    { 0,0,0 },
    { GL_LUMINANCE, GL_LUMINANCE, GL_UNSIGNED_BYTE },
    { GL_RGB,       GL_RGB,       GL_UNSIGNED_SHORT_5_6_5 },
    { GL_RGB,       GL_RGB,       GL_UNSIGNED_BYTE },
    { GL_RGBA,      GL_RGBA,      GL_UNSIGNED_BYTE }
};
*/


int nfillers  = 2;
int nbufseach = 4;
int readsize  = 8388608;
// int readsize = 65536; // 8388608;    // 8MB

int port = 0;

char *prog;

char *controlfname;

char *runcmdfmt = NULL;
int runcmdms = -300;

enum stereomode { UNSPEC, MONO, CROSSEYED, QUADBUFFERED };

enum stereomode stereo = UNSPEC;


	/* State variables */
int winx, winy;		/* current window size */
int newtex = 1;		/* newly minted textures? (need full init rather than subload) */

int paused = 1;		/* Movie paused? */
int doloop = 1;
int majorloop = 1;
int playfwd = 1;
int quitnow = 0;
int rateoverride = 0;	/* Should defaultrate override preferred-rate setting in movie? */

float defaultrate = 20;	/* default target frames/sec */
float currate;
float framems = 50;	/* target milliseconds per frame */
int msfudge = 35;	/* framems = 1000/rate - msfudge */
int verbose = 0;
int skipby = 1;		/* skip -- show every Nth frame */
int preloadms = 0;	/* After switching movies, wait this long for startup */
int shiftoverride = 0;
int defaultshift = 0;
int relshift = 0;

int globaltiming = 1;	/* using global clock? (want this for external media sync) */
int globalstartms;	/* timems() as of when this clock-run started */
int globalframes;	/* frame count since globalstartms time */
float globalframems;	/* ms per frame at current rate */

static int swapinterval = -1;

bpio_t	   *playlist;
bpmvhead_t *mvlist;
int	   nplay;
int	   curplay;

float accumdt = 0, accumweight = 0;

void resumemovie( int loop );
void pausemovie();
int openmovie( bpio_t *bpio, char *fname, bpmvhead_t *bpmv );

# define glutPostRedisplay()  /* dummy */
# define glutIdleFunc( argh )  /* dummy */
# define glutTimerFunc( a, b, c )  /* dummy */


off_t bpstartpos( bpio_t *bpio )
{
    return bpio==NULL ? 0 : bpio->bpb[0].wrappos;
}

off_t bpendpos( bpio_t *bpio )
{
    return bpio==NULL ? 0 : bpio->bpb[0].eofpos;
}

off_t bpincrpos( bpio_t *bpio )
{
    return bpio==NULL ? 0 : bpio->bufsize;
}


int bpawaitone( bpio_t *bpio )
{
    bpbuf_t *bpb = &playlist[curplay].bpb[ bpio->drain ];

    pthread_mutex_lock( &bpb->bmut );
    while(bpbempty( bpb ) && bpb->filling) {
        bpb->drainwaiting = 1;
        pthread_cond_wait( &bpb->bdrainwait, &bpb->bmut );
        bpb->drainwaiting = 0;
    }
    pthread_mutex_unlock( &bpb->bmut );

    return !bpbempty(bpb);
}

int bpconsume1( bpio_t *bpio )
{
    bpbuf_t *bpb =  &bpio->bpb[ bpio->drain ];

    if(!bpb->filling)	/* if EOF or bpstop() or etc. */
        return 0;

    pthread_mutex_lock( &bpb->bmut );
    bpb->wp = (bpb->wp + 1) % bpb->nbufs;

    if(bpb->fillwaiting)
        pthread_cond_signal( &bpb->bfillwait );
    pthread_mutex_unlock( &bpb->bmut );

    bpio->drain = (bpio->drain+1) % bpio->nfillers;

    return bpawaitone( bpio );
}

int bpframeof( bpio_t *bpio, off_t pos )
{
    off_t incr = bpio->bpb[0].incrpos;
    return incr > 0 ? (pos - bpstartpos(bpio)) / incr :
	   incr < 0 ? (pos - bpstartpos(bpio)) / (-incr) :
	   -1;
}

float defaultratefor( int curplay )
{
    return rateoverride
	 || !(mvlist[curplay].flags & BPF_PREFRATE)
	 || mvlist[curplay].prefrate == -1  /* bug in some versions of img2bmv */
	 ? defaultrate : mvlist[curplay].prefrate;
}

int defaultshiftfor( int curplay )
{
    return shiftoverride || !(mvlist[curplay].flags & BPF_PREFSHIFT)
		? defaultshift : mvlist[curplay].prefshift;
}

int lasttimems;

int timems()
{
    static struct timeval then;
    struct timeval now;

    gettimeofday( &now, NULL );
    if(then.tv_sec == 0)
	then = now;
    return (now.tv_sec - then.tv_sec) * 1000 + (now.tv_usec - then.tv_usec) / 1000;
}


static int subpipe;

void runcommand( char *fmt, bpio_t *bpio, int msfromnow )
{
    char *bpf = bpfname(bpio);
    int bpflen = strlen(bpf);
    int room = strlen(fmt) + 4*strlen(bpf) + 10;
    char *cmd = (char *)malloc(room);
    char *src = strdup(bpf);
    int pip[2];
    int kid;
    char c;

    if(fmt[0] == '-')
	return;

    if(bpflen > 4 && 0==strcmp(bpf+bpflen-4, ".bmv"))
	bpflen -= 4;
    src = (char *)malloc(bpflen+1);
    memcpy(src, bpf, bpflen);
    src[bpflen] = '\0';

    sprintf(cmd, fmt, src, src, src, src);	/* in case of multiple %s's */

    pipe( pip );

    if(subpipe != 0)		/* tell any previously-running command to quit */
	close(subpipe);

    subpipe = pip[1];		/* we hold the writing end of the pipe */

    kid = fork();
    if(kid < 0) {
	perror("fork");
	return;
    }
    if(kid == 0) {
	close(pip[1]);
	setsid();
	if(msfromnow > 0)
	    usleep(msfromnow * 1000);

	if(fork() == 0) {
	    execl("/bin/sh", "sh", "-xc", cmd, NULL);
	    perror("exec /bin/sh");
	    _exit(1);
	} else {
	    signal(SIGTERM, SIG_IGN);	/* don't kill self yet */
	    signal(SIGTERM, SIG_IGN);	/* don't kill self yet */
	    signal(SIGHUP, SIG_IGN);	/* don't kill self yet */
	    signal(SIGQUIT, SIG_IGN);	/* don't kill self yet */
	    signal(SIGALRM, SIG_IGN);	/* don't kill self yet */
	    read(pip[0], &c, 1);	/* wait until parent closes its end of the pipe */
	    killpg(0, SIGTERM);		/* kill everybody else */
	    sleep(1);
	    killpg(0, SIGKILL);		/* now kill entire subsession */
	    _exit(0);
	}
    }
    /* or, if we're the parent, just return. */
}

void globalclock( float rate )
{
    if(rate != 0)
	globalframems = (rate > 0) ? 1000 / rate : -rate;
    globalstartms = timems();
    globalframes = 0;
}

int playlistadvance( int sign )
{
    bpio_t *bpio;
    int waspaused = paused;
    int oldplay = curplay;

    if(curplay >= 0)
        pausemovie();

    curplay += sign;

    if(majorloop)
        curplay = (curplay + nplay) % nplay;

    if(curplay < 0 || curplay >= nplay) {
        pausemovie();
        return 0;
    }

    if(oldplay != curplay) {
	currate = defaultratefor( curplay );
	framems = currate > 0 ? 1000 / currate - msfudge : -currate;

        relshift = defaultshiftfor( curplay );
    }

    bpio = &playlist[curplay];

    bpforward( bpio, playfwd * skipby );
    bpsync( bpio );
    bpseek( bpio, playfwd>0 ? bpstartpos( bpio )
            : bpendpos( bpio ) - bpio->bufsize * skipby );

    if(verbose >= 2)
	fprintf(stderr, "advance %d  framems %g\n", sign, framems);

    if(!waspaused)
        resumemovie( doloop );

    if(preloadms) {
	if(runcmdfmt) {
	    runcommand( runcmdfmt,
		bpio, preloadms + runcmdms/* presumably negative */);
	}
	usleep( preloadms * 1000 );
    }

    globalclock( currate );

    return 1;
}

void idler(void);

void showlater( int junk )
{
    glutPostRedisplay();
    glutIdleFunc( paused ? NULL : idler );
}


void idler()
{
    if(quitnow) {
        exit(1);
    }
    if(verbose>=3) { printf("I"); fflush(stdout); }

    if(bpconsume1( &playlist[curplay] )) {
	/* Got a buffer ready.  Should we show it now? */
	if(swapinterval >= 0 && !globaltiming) {
	    /* Rather than timing here, we'll depend on glXSwapIntervalSGI() */
	    glutPostRedisplay();
	} else {
	    /* Nope, we do our own timing */
	    int now = timems();
	    int target = (int)(lasttimems + framems);
	    if(globaltiming) {
		/* try our best to maintain sync with a target clock */
		target = (int) (globalframems * globalframes + globalstartms);
	    }
	    if(now < target) {
		usleep( (target - now) * 1000 );
	    }
	}
    } else {
        if( ! playlistadvance( playfwd ) )
        {
                // glutIdleFunc( NULL );
        }
    }
}

void catch_quit(int sig) {
    quitnow = 1;
}

int ntimes = 50;

void resumemovie( int loop )
{
    bpio_t *bpio = &playlist[curplay];
    if(verbose>=3) printf("resume %d p%d\n", loop, paused);
    bpstart( bpio, loop );
    if(!bpawaitone( bpio )) {
	int isfwd = (bpio->bpb[0].incrpos > 0);
	if(verbose) printf("resume: resetting to %s\n", isfwd ? "end" : "beginning");
	bpseek( bpio, isfwd ? bpendpos(bpio) : bpstartpos(bpio) );
	bpstart( bpio, loop );
	if(!bpawaitone(bpio)) {
	    printf("resume: bpawaitone() -> 0?\n");
	}
    }

    glutPostRedisplay();
    if(paused) {
	glutIdleFunc( idler );
	globalclock( currate );		/* want to reset clock whether glutted or not */
    }

    paused = 0;
}

void startmovie( int loop )
{
    if(verbose>=2) printf("start %d\n", loop);
    bpseek( &playlist[curplay], bpstartpos( &playlist[curplay] ) );
    bpsync( &playlist[curplay] );
    resumemovie( loop );
}

void pausemovie()
{
    glutIdleFunc( NULL );
    paused = 1;
}

static int val, hasnum;

int getval( int def ) {
    return hasnum < 0 ? -val : hasnum > 0 ? val : def;
}

void kb( unsigned char key, int x, int y );


void kb( unsigned char key, int x, int y )
{
    int isdigit = 0;
    bpio_t *bpio = &playlist[curplay];
    int ms;

    switch(key) {
	case '\033':
	    exit(0);

	case ' ':
	    paused = !paused;
	    if(paused)
		pausemovie();
	    else
		resumemovie( doloop );
	    break;

	case 'p':
	    pausemovie();
	    break;

	case '.': case '>':
	    bpstop( bpio );
	    bpsync( bpio );
	    bpforward( bpio, skipby );
	    resumemovie( doloop );
	    bpconsume1( bpio );
	    pausemovie();
	    break;

	case ',': case '<':
	    bpstop( bpio );
	    bpsync( bpio );
	    bpforward( bpio, -skipby );
	    resumemovie( doloop );
	    bpconsume1( bpio );
	    pausemovie();
	    break;

	case '-':
	    hasnum = -1;
	    isdigit = 1;
	    break;

	case 'n':
	    if(hasnum)
		curplay = getval(0) - 1;
	    playlistadvance( 1 );
	    break;

	case 'N':
	    playlistadvance( -1 );
	    break;

	case 'P':
	    ms = getval( 0 );
	    if(ms > 0) preloadms = ms;
	    else if(ms < 0) runcmdms = ms;
	    break;

	case 'f':
	    playfwd = 1;
	    bpforward( bpio, playfwd * skipby );
	    bpsync( bpio );
	    resumemovie( doloop );
	    break;

	case 'b':
	    playfwd = -1;
	    bpforward( bpio, playfwd * skipby );
	    bpsync( bpio );
	    resumemovie( doloop );
	    break;

	case 'l':
	    doloop = hasnum ? val : !doloop;
	    if(doloop != 0)
		playfwd = doloop;
	    pausemovie();
	    resumemovie( doloop );
	    break;

	case 'R':
	    relshift = getval( relshift+1 );
	    glutPostRedisplay();
	    if(verbose>=1) printf("relshift %d\n", relshift);
	    break;

	case 'L':
	    relshift = -getval( -(relshift-1) );
	    glutPostRedisplay();
	    if(verbose>=1) printf("relshift %d\n", relshift);
	    break;


	case 'z':
	    playfwd = 1;
	    skipby = 1;
	    bpforward( bpio, playfwd * skipby );
	    bpsync( bpio );
	    startmovie( doloop );	/* schedule first frame */
	    pausemovie();
	    break;

	case 'v':
	    verbose = getval( verbose ? 0 : 2 );
	    glutPostRedisplay();
	    break;

	case 'g': {
		int frameno = getval(0);
		int nframes = mvlist[curplay].nframes;
		if(frameno >= nframes-1)
		    frameno = nframes-1;
		else if(frameno < 0)
		    frameno = 0;
	    
            bpseek( bpio, bpstartpos( bpio ) + frameno * (off_t) bpio->bufsize );
            bpsync( bpio );
            resumemovie( doloop );
            bpawaitone( bpio );
            pausemovie();
	    }
            break;

        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            val = val*10 + key - '0';
            hasnum = 1;
            isdigit = 1;
            break;

  
	case 's': {
		float newrate = getval( (int)defaultratefor(curplay) );
		int newframems = (int) (newrate>0 ? 1000.0/newrate - msfudge : -newrate);

		if(newframems != framems) {
		    framems = newframems;
		    currate = newrate;
		    globalclock( newrate );
		}
	    }
	    if(verbose>=2)
		fprintf(stderr, "%d fps -> allot %g ms/frame with %dms fudge\n",
			val, framems, msfudge);
	    break;

	case 't':
	    framems = getval( (int)framems );
	    break;

	case 'S':
	    skipby = getval( 1 );
	    bpforward( bpio, playfwd * skipby );
	    break;

	case 'T':
	    {
		int newtiming = getval(1);
		if(newtiming == 0) {
		    globaltiming = 0;
		} else if(newtiming == 1 && globaltiming == 0) {
		    if(verbose)
			printf("Enabling global timing\n");
		    globaltiming = 1;
		    globalclock( currate );
		}
		break;
	    }

	case '=': {
	    int frameno = ( bptell( bpio ) - bpstartpos( bpio ) ) / bpincrpos( bpio );
	    float fps = accumdt==0 ? 0 : 1000 * accumweight / accumdt;
	    fprintf(stderr, "frame %04d   %5.1f fps  relshift %d\n", frameno, fps, relshift);
            if(verbose>=2)
                fprintf(stderr, "%d fps -> allot %d ms/frame with 10ms fudge\n", val, framems);
          }
	  break;

	case 'h':
	case '?':
	  fprintf(stderr, "bplay keyboard commands:\n\
  <NNN>s   aim for NNN frames/sec [%gs]  or <NNN>m aim for NNN ms/frame [%gm]\n\
  <NNN>S   skip -- show every NNN'th frame [%dS]\n\
  <NNN>g   go to NNN'th frame and pause (first = 0)\n\
   .       one frame forward\n\
   ,       one frame backward\n\
   f       run forward\n\
   b       run backward\n\
   v       toggle verbose (on-screen framenumber & fps counter) [%dv]\n\
   p       pause\n\
   l	   toggle loop/stop-at-end\n\
  <0|1>T   global-timing option [%dT]\n\
  SPACE    toggle run/pause [%s]\n\
  HOME     go to beginning of movie and play (like \"0gf\")\n\
  R L ctrl-RIGHT ctrl-LEFT  adjust stereo separation [%dR]\n\
  <NNN>P   Preload time (delay when starting/switching movies) [%dP]\n\
  -<NNN>P  Pre-audio offset (%dP)\n",
	currate, framems, skipby, verbose, globaltiming,
	paused ? "paused" : "running",
	relshift, preloadms, runcmdms);

	  break;
    }
    if(!isdigit) {
        if(verbose>=2) {
            if(hasnum)	printf("\"%d%c\"\n", getval(0), key);
            else	printf("\"%c\"", key);
        }
        val = hasnum = 0;
    }
}

void swab32( void *ap, int nbytes )
{
    unsigned int *p = (unsigned int *)ap;
    while((nbytes -= 4) >= 0) {
        unsigned int v = *p;
        *p++ = ((v>>24)&0xFF) | ((v>>8)&0xFF00) | ((v&0xFF00)<<8) | ((v&0xFF)<<24);
    }
}

void swabbpmv( bpmvhead_t *bpmv )
{
    swab32( bpmv, (char *)&bpmv->extfname - (char *)bpmv );

        /* bpmv->start is a 64-bit quantity and needs its words swapped */
    bpmv->start = ((bpmv->start & 0xFFFFFFFFLL) << 32)
		| ((bpmv->start >> 32) & 0xFFFFFFFFLL);
}


int openmovie( bpio_t *bpio, char *fname, bpmvhead_t *bpmv )
{
    int fd;
    int n;
    bpmvhead_t head;

    if(fname == NULL)
        return -1;

    fd = open(fname, O_RDONLY);

    if(fd < 0) {
        fprintf(stderr, "%s: %s: can't open: %s\n",
                prog, fname, strerror(errno));
        return -1;
    }

    n = read( fd, &head, sizeof(head) );
    if(n <= &head.extfname[0] - (char *)&head) {
        fprintf(stderr, "%s: %s: can't read header: %s\n",
                prog, fname, strerror(errno));
        return -1;
    }

    if(head.magic != BPMV3_MAGIC)
	swabbpmv( &head );
    if(head.magic != BPMV3_MAGIC) {
	fprintf(stderr, "%s: %s doesn't look like a bpmovie file (as made by img2bmv).\n",
		prog, fname);
	return -2;
    }

    close(fd);

        /* OK then. */

    newtex = 1;

    if(bpio->nfillers == 0)
        bpinit( bpio, nfillers, head.imagestride, readsize, nbufseach );

    if(head.flags & BPF_EXTDATA) {
        if(bpopen( bpio, head.extfname ) < 0) {
            fprintf(stderr, "%s: %s refers to movie data in \"%s\": %s\n",
                    prog, fname, head.extfname, strerror(errno));
            return -1;
        }
    } else {
        if(bpopen( bpio, fname ) < 0)
            return -1;
    }

    if(bpmv)
        *bpmv = head;

    if(head.flags & BPF_EXTDATA) {
        off_t end = head.start + (long long)head.imagestride * (long long)head.nframes;
        bprange( bpio, head.start, end );
    } else {
        bprange( bpio, head.start, -1LL );
    }
    bpseek( bpio, head.start );
    bpsync( bpio );

    return 0;
}


void draweye( bpmvhead_t *bpmv, unsigned int *txs, unsigned char *tilebuf,
              float scl, float imxsz, float xoff, float yoff, int x0, int x1, float *xagain )
{
    int i,j,k, ii;

    for(i = k = 0; i < bpmv->nytile; i++) {
        for(j = x0; j < x1; j++, k++) {

            float xx0 = xoff + scl * j * bpmv->xtile;
            float xx1 = xoff + scl * (j+1) * bpmv->xtile;
            float yy0 = yoff + scl * i * bpmv->ytile;
            float yy1 = yoff + scl * (i+1) * bpmv->ytile;

                /*
                  fprintf(stderr, "%d %d %d %d - stride %d %d %d\n",
                  j * bpmv->xtile,
                  (j+1) * bpmv->xtile,
                  i * bpmv->ytile,
                  (i+1) * bpmv->ytile,
                  bpmv->xtilestride,
                  bpmv->ytilestride, bpmv->tilerowstride);
                */


	    unsigned char *ptr, *dst;

	    switch( bpmv->format ) {
	    case RGB888:
		ptr = tilebuf + j*bpmv->xtilestride + i*bpmv->ytilestride;
		dst = rgbBuffer + i*bpmv->xtile*x1*bpmv->ytile*3 + j*bpmv->xtile*3;
		for (ii=0;ii<bpmv->ytile;ii++)
		{
		    memcpy(dst, ptr, bpmv->xtile * 3);
		    dst += bpmv->xtile * x1 * 3;
		    ptr += bpmv->xtile * 3;
		}
		break;

	    case RGB565:
		ptr = tilebuf + j*bpmv->xtilestride + i*bpmv->ytilestride;
		dst = rgbBuffer + i*bpmv->xtile*x1*bpmv->ytile*2 + j*bpmv->xtile*2;
		for (ii=0;ii<bpmv->ytile;ii++)
		{
		    memcpy(dst, ptr, bpmv->xtile * 2);
		    dst += bpmv->xtile * x1 * 2;
		    ptr += bpmv->xtile * 2;
		}
		break;

	    default:
		fprintf(stderr, "bplay-noglut: can only handle 24-bit (-p 3) and 16-bit (-p 2) .bmv movies, not %d\n", bpmv->format);
		exit(1);

	    }

        }
    }
    
    swapWithBuffer(sageInf, rgbBuffer);
}

void redraw()
{
    static unsigned int *txs = NULL;
    static int ntxs = 0;
    static float msdrift;

    bpmvhead_t *bpmv = &mvlist[curplay];
    float xscl, yscl, scl, eyex, imxsz, xoff, yoff;
    float xright;

    unsigned char *tilebuf;
    int now;

    float dt, decay;

    tilebuf = bpcurbuf( &playlist[curplay] );

    eyex = (stereo==CROSSEYED) ? winx/2 : winx;
    xright = (stereo==CROSSEYED) ? winx / 2 : 0;
    imxsz = (bpmv->flags&BPF_STEREO) ? bpmv->xsize/2 : bpmv->xsize;
    xscl = eyex / (float)imxsz;
    yscl = winy / (float)bpmv->ysize;
    scl = (xscl < yscl) ? xscl : yscl;
    xoff = 0.5f * (eyex - scl * imxsz);
    yoff = 0.5f * (winy - scl * bpmv->ysize);

    if(verbose >= 5)
	fprintf(stderr, "s%.3g xo%g yo%g\n", scl, xoff, yoff);

    if(verbose<4) {
	int stereomovie = bpmv->flags & BPF_STEREO;
	float xoff = 0.5f * (eyex - scl * imxsz);
	float yoff = 0.5f * (winy - scl * bpmv->ysize);
	int nxtile = bpmv->nxtile;

	switch(stereo) {
	case UNSPEC:
	case MONO:
	    /*
	     * If it's a stereo movie and we're in mono mode,
	     * just display the left half.
	     */
	    draweye( bpmv, txs, tilebuf, scl, imxsz, xoff, yoff,
			0, stereomovie ? nxtile/2 : nxtile, NULL );
	    break;

	case CROSSEYED:
	    if(stereomovie) {
		draweye( bpmv, txs, tilebuf, scl, imxsz, xoff+relshift, yoff, 0, nxtile/2, NULL );
		draweye( bpmv, txs, tilebuf, scl, imxsz, xright+xoff-relshift, yoff, nxtile/2, nxtile/2, NULL );
	    } else {
		/* mono movie in stereo mode -- draw twice */
		draweye( bpmv, txs, tilebuf, scl, imxsz, xoff, yoff, 0, nxtile, &xright );
	    }
	    break;
	}
    }

    if(swapinterval < 0) {
	/* if we can't depend on glXSwapIntervalSGI() for timing */
	now = timems();
    }

    globalframes++;


    now = timems();
    dt = (now - lasttimems);
    decay = exp( - dt / 100 );
    accumdt =     (accumdt * decay)     + dt;
    accumweight = (accumweight * decay) + 1;

    lasttimems = now;
    msdrift = globalframems * globalframes + globalstartms - now;

    if(verbose >= 2) {
	bpio_t *bpio = &playlist[curplay];
	bpbuf_t *bpb = &bpio->bpb[ bpio->drain ];
	int frameno = tilebuf ? *(int *)( tilebuf + bpmv->imagestride - 4) : -9999; /* img2bmv easter egg */
	printf("<%02d:%02d~%02d> f%d fw%d r%d dw%d w%d %d..%d  %.0fms %.0fmsdrift\n",
		bpio->drain,
		bpb->curpos[bpb->wp] < 0 ? -1 : (int) (bpb->curpos[bpb->wp] / bpio->bufsize),
		frameno,
		bpb->filling,
		bpb->fillwaiting, bpb->rp, bpb->drainwaiting, bpb->wp,
		(int) (bpb->wrappos / bpio->bufsize),
		(int) (bpb->eofpos / bpio->bufsize),
		dt,
		msdrift
		);
	fflush(stdout);
    }

    newtex = 0;

}

void visible( int yes )
{
    static int first = 1;
    if(verbose >= 2) printf("visible(%d) [%d]\n", yes, first);
    if(yes && first) {
	startmovie( doloop );
	first = 0;
    } else if(!yes) {
	pausemovie();
    }
}

char *myversion() {
    int len;
    char *e, *s = strchr(id, ',');
    static char vers[32];

    if(s) s = strchr(s, ' ');
    if(s == NULL) return (char*)"";
    s++;
    e = strchr(s, ' ');
    len = e ? e - s : strlen(s);
    if(len > 31) len = 31;
    memcpy(vers, s, len);
    vers[len] = '\0';
    return vers;
}
    

int main(int argc, char *argv[])
{
    int c, i;
    char *hostIP;
    
    static char Usage[] = "Usage: %s [options] file.bmv ...\n\
Plays uncompressed bulk movies in the format created by img2bmv.\n\
Options: [version %s]\n\
   -f NNN	target NNN frames/sec or -f NNNm milliseconds/frame\n\
   -t NTHREADS	number of reader threads\n\
   -r READSIZE	size of each read() in bytes (default = image size)\n\
   -M msfudge	fudge factor for ms/frame timing estimates (default %d)\n\
   -v		verbose\n\
   -R \"command ... %%s\"  start command along with video.  %%s => moviename (no .bmv)\n\
   -P -advance	start -R command \"-advance\" millisecs before video (-P -300 default)\n\
   -T 1-or-0	sync to global clock (-T 1, default) or don't (-T 0).\n\
   -m / -C / -q Play in mono / crosseyed-stereo / quadbuffered-stereo mode\n\
   -c hostIP    SAGE connection to given host [?]\n\
";

    prog = argv[0];

    signal(SIGINT, catch_quit);
    setlinebuf(stdout);

    hostIP = NULL;

    while((c = getopt(argc, argv, "c:t:b:r:p:S:F:f:V:P:L:R:vCmqM:R:T:")) != EOF) {
	switch(c) {
            case 'c':
                hostIP = strdup(optarg);
                break;
                
	    case 'R':
		globaltiming = 1;
		runcmdfmt = optarg;
		break;

	    case 'T':
		globaltiming = atoi(optarg);
		break;

	    case 't':
		nfillers = atoi(optarg);
		if(nfillers < 1) {
		    fprintf(stderr, "%s: Need at least 1 reader thread\n",
			    prog);
		    exit(1);
		}
		break;

	    case 'b':
		nbufseach = atoi(optarg);
		if(nbufseach < 2) {
		    fprintf(stderr, "%s: Need at least 2 buffers per reader thread\n",
			    prog);
		    exit(1);
		}
		break;

	    case 'r':
		readsize = atoi(optarg);
		if(readsize <= 0 || readsize % getpagesize() != 0) {
		    fprintf(stderr, "%s: readsize must be a multiple of pagesize (%d)\n",
			    prog, getpagesize());
		    exit(1);
		}
		break;

	    case 'p':
		port = atoi(optarg);	/* Not implemented yet */
		break;

	    case 'P':
		if(strchr(optarg, '/') || optarg[0] == '-') {
		    sscanf(optarg, "%d/%d", &runcmdms, &preloadms);
		} else {
		    preloadms = atoi(optarg);
		}
		break;

	    case 'M':
		msfudge = atoi(optarg);
		break;

	    case 'S':
		skipby = atoi(optarg);
		if(skipby <= 0) skipby = 1;
		break;

	    case 'F':
		rateoverride = 1;	/* and fall into ... */
	    case 'f':
	      {
            double v = atof(optarg);
            fprintf(stderr, "Default rate %lf\n", v);
            if(strchr(optarg, '!'))
                rateoverride = 1;
            if(strchr(optarg, 'm'))
                defaultrate = (int) -v;
            else
                if(v>0)
                 defaultrate = (int) v;
                else {
                    fprintf(stderr, "%s: -f %s: expected frame rate (frames/sec) or frame time in milliseconds (with \"m\" suffix).\n",
                    prog, optarg);
                    exit(1);
                }
	      }
	      break;

	    case 'C':
	      stereo = CROSSEYED;
	      break;

	    case 'm':
	      stereo = MONO;
	      break;

	    case 'q':
	      stereo = QUADBUFFERED;
	      break;

	    case 'L':
	      defaultshift = -atoi(optarg);
	      shiftoverride = (strchr(optarg, '!') != NULL);
	      break;

	    case 'v':
	      verbose++;
	      break;

	    default:
		fprintf(stderr, "%s: unknown option -%c\n", prog, c);
		fprintf(stderr, Usage, prog, myversion(), msfudge);
		exit(1);
		break;
	}
    }

    nplay = argc - optind;
    playlist = (bpio_t *) calloc( nplay, sizeof(bpio_t) );
    mvlist = (bpmvhead_t *) calloc( nplay, sizeof(bpmvhead_t) );

    for(i = 0; i < nplay; i++) {
        if( openmovie( &playlist[i], argv[i+optind], &mvlist[i] ) < 0 ) {
            fprintf(stderr, "%s: %s: cannot open: %s\n",
                    prog, argv[i+optind], strerror(errno));
            exit(1);
        }
    }

    if(nplay <= 0 && port == 0) {
        fprintf(stderr, Usage, prog, myversion(), msfudge);
        exit(1);
    }

         // ----------------------------    
	
    
    int stereomovie = mvlist[0].flags & BPF_STEREO;
    if (stereomovie)
        fprintf(stderr, "It's a Stereo Movie \n");
    else 
        fprintf(stderr, "It's a Mono Movie \n");

    int imgwidth  = mvlist[0].nxtile*mvlist[0].xtile;        
    int imgheight = mvlist[0].nytile*mvlist[0].ytile;

    rgbBuffer = new unsigned char[imgwidth*imgheight*3];
    memset(rgbBuffer, 0, imgwidth*imgheight*3);

    switch( mvlist[0].format ) {
        case RGB888:
            sageInf = createSAIL("bitplayer", imgwidth, imgheight, PIXFMT_888_INV, hostIP, (int)defaultrate);
            fprintf(stderr, "App created %d x %d (%d fps)\n", imgwidth, imgheight, (int)defaultrate);
        break;

        case GRAY8:
        case RGB565:
        case ABGR8888:
        case COMPRESSDXT1:
            fprintf(stderr, "Pixel format not supported yet\n");
            exit(1);
        break;
    }



    // ----------------------------    

    curplay = -1;
    playlistadvance(1);

    startmovie( doloop );

    while (1)
    {
        redraw();
        idler();
        //usleep(framems*1000);
        //usleep(1);
    }
    
    return 0;
}
