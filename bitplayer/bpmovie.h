#ifndef BPMOVIE_H
#define BPMOVIE_H 1

/*
 * NCSA, University of Illinois Urbana-Champaign, 2004
 * Stuart Levy, slevy@ncsa.uiuc.edu
 */

#define	BPMV3_MAGIC   ( ('b'<<24) | ('p'<<16) | ('m'<<8) | '3' )

enum pixeltype {
    GRAY8    = 1,	/* 8-bit grayscale */
    RGB565   = 2,	/* 16-bit (5-bit-R)<<11 | (6-bit-G)<<5 | (5-bit-R) */
    RGB888   = 3,	/* 8-bit R, G, B bytes in that order */
    ABGR8888 = 4,	/* 8-bit A, B, G, R bytes in that order */
    COMPRESSDXT1 = 5	/* S3TC DXT1 compressed texture (16 texels in 8 bytes) */
};

typedef struct {
    int magic;		/* 32-bit int BPMV_MAGIC in native byte order */
    int xsize;		/* image size */
    int ysize;
    enum pixeltype format;
    int imagestride;	/* bytes per image, including page-alignment padding */


    int xtile;		/* X-direction size of each tile */
    int ytile;		/* Y-direction size of each tile */
    int nxtile;		/* number of tiles in X-direction */
    int nytile;		/* number of tiles in Y-direction */
    int tilerowstride;	/* spacing (bytes) from one row to next within a tile */
    int xtilestride;	/* spacing (bytes) from one tile to next in X */
    int ytilestride;	/* spacing (bytes) from one tile to next in Y */

    int nframes;	/* number of frames in movie, if known, else MAXINT */

    int flags;		/* movie-type flags */
#define BPF_EXTDATA 0x01	/* Image data in separate file 'extfname' */
#define BPF_STEREO  0x02	/* Each frame is a stereo pair */
#define BPF_PREFRATE 0x04	/* Has "preferred play rate" setting */
#define BPF_PREFSHIFT 0x08	/* Has "preferred relative image shift" setting */

    long long start;	/* file offset of first image data */

    int prefrate;	/* preferred play rate, frames/sec */
    int prefshift;	/* preferred relative image shift, pixels */

    char pad[56];	/* some room for extensions */

    char extfname[1024]; /* name of file with image data, if flags&BPF_EXTDATA */
    char cmd[1024];	/* command used to generate this */


} bpmvhead_t;

#endif
