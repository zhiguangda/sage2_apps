/*
 *	getpix and putpix -
 *
 *				Paul Haeberli - 1984
 *
 */
#include	<stdio.h>
#include	<errno.h>
#include	"image.h"

#undef getpix
#undef putpix

IMushort getpix( IMAGE *image )
{
    if(--(image)->cnt>=0)
    	return *(image)->ptr++;
    else
	return ifilbuf(image);
}

IMushort putpix( IMAGE *image, IMushort pix )
{
    if(--(image)->cnt>=0)
        return *(image)->ptr++ = pix;
    else
	return iflsbuf(image,pix);
}
