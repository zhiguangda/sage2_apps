/*
 * Given a PBM/PGM/PPM image file, return a DISPIMAGE
 * containing its raster data and with size <= (xsize, ysize).
 * Also accepts filenames beginning with "|"; they are shell commands,
 * fed to popen().
 */

#include <stdio.h>
#include <stdlib.h>
#include "imginfo.h"

extern int pnm_getint( FILE * );

IMG *
pnmmakedisp(filename)
    char *filename;
{
    register FILE *f;
    IMG *di = NULL;
    int maxpix = 0, pnmkind;
    int xsize, ysize;
    int y;
    int c;
    int ispipe = (filename[0] == '|');


    f = ispipe ? popen(filename+1, "r") : fopen(filename, "rb");
    if(f == NULL) {
	fprintf(stderr, "%s: cannot open: ", filename);
	perror("");
	return NULL;
    }

    if(fgetc(f) != 'P')
	goto fail;

    pnmkind = fgetc(f);

    xsize = pnm_getint(f);
    ysize = pnm_getint(f);
    switch(pnmkind) {
    case '1': case '4':
	maxpix = 1;
	break;
    case '2': case '5': /* fall into ... */
    case '3': case '6':
	maxpix = pnm_getint(f);
	break;
    default:
	goto fail;
    }
    if(xsize <= 0 || ysize <= 0 || maxpix <= 0)
	goto fail;

    if(pnmkind >= '4') {
	/* Skip to end of line, after which the binary data should follow */
	while((c = fgetc(f)) != EOF && c != '\n')
	    ;
    }

    di = (IMG *)malloc(sizeof(IMG));
    if(pnmkind == '1' || pnmkind == '4') {
	di->type = IT_BIT;
	di->rowbytes = (xsize+31)/32 * 4;
    } else {
	di->type = IT_LONG;
	di->rowbytes = xsize*4;
    }
    di->xsize = xsize;
    di->ysize = ysize;
    di->data = malloc(di->rowbytes * ysize);
    if(di->data == NULL) {
	fprintf(stderr, "Can't malloc %d bytes of memory for image\n",
	    di->rowbytes*ysize);
	exit(2);
    }
    for(y = ysize; --y >= 0; ) {
	register int v;
	register int *rp;
	register int x;

	rp = (int *)(di->data + y*di->rowbytes);
	x = xsize;
	switch(pnmkind) {
	case '1':
	    do {
		*rp++ = 0xff000000 | -pnm_getint(f);
	    } while(--x > 0);
	    break;
	case '2':
	    do {
		v = (255 * pnm_getint(f) / maxpix);
		*rp++ = 0xff000000 | (v << 16) | (v<<8) | v;
	    } while(--x > 0);
	    break;
	case '3':
	    do {
		v = (255 * pnm_getint(f) / maxpix);
		v |= (255 * pnm_getint(f) / maxpix) << 8;
		*rp++ = v | (255 * pnm_getint(f) / maxpix) << 16;
	    } while(--x > 0);
	    break;
	case '4':
	    fread(rp, (xsize+7)/8, 1, f);	/* Bit mode */
	    break;
	case '5':
	    if(maxpix != 255) {
		do {
		    v = 255 * getc(f) / maxpix;
		    *rp++ = 0xff000000 | (v << 16) | (v << 8) | v;
		} while(--x > 0);
	    } else {
		do {
		    v = getc(f);
		    *rp++ = 0xff000000 | (v << 16) | (v << 8) | v;
		} while(--x > 0);
	    }
	    break;
	case '6':
	    if(maxpix != 255) {
		do {
		    v = 255 * getc(f) / maxpix;
		    v |= (255 * getc(f) / maxpix) << 8;
		    *rp++ = 0xff000000 | ((255 * getc(f) / maxpix) << 16) | v;
		} while(--x > 0);
	    } else {
		do {
		    v = getc(f);
		    v |= getc(f) << 8;
		    *rp++ = 0xff000000 | (getc(f) << 16) | v;
		} while(--x > 0);
	    }
	    break;
	}
    }
		
    if(f != NULL) {
	if(ispipe) pclose(f);
	else fclose(f);
    }
    return di;

  fail:
    if(di) {
	if(di->data) free(di->data);
	free(di);
    }
    if(f != NULL) {
	if(ispipe) pclose(f);
	else fclose(f);
    }
    return NULL;
}
