#include <stdio.h>
#include <stdlib.h>
#include "tiffio.h"
#include "imginfo.h"
#include <sys/types.h>


#define	rgba(r, g, b, a)	((a)<<24 | (b)<<16 | (g)<<8 | (r))
#define	rgbi(r, g, b)		(0xFF000000 | (b)<<16 | (g)<<8 | (r))

IMG *
TIFFmakedisp( char *fname )
{
	TIFF *tif;
	register IMG *im;
	uint32 width, height;

	tif = TIFFOpen(fname, "r");
	if(tif == NULL)
	    return NULL;

	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
	im = (IMG *)malloc(sizeof(IMG));
	im->type = IT_LONG;
	im->rowbytes = width * sizeof(long);
	im->xsize = width;
	im->ysize = height;
	im->data = (unsigned char *)malloc(im->rowbytes * height);
	if(im->data == NULL) {
	    fprintf(stderr, "Can't malloc %ld bytes of memory for image\n",
		(long) (im->rowbytes * height));
	    exit(2);
	}
	if(TIFFReadRGBAImage(tif, width, height, (uint32 *)im->data, 0) == 0) {
	    free(im->data);
	    free(im);
	    im = NULL;
	}
	TIFFClose(tif);
	return im;
}
