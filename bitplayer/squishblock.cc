#include <stdlib.h>

#include "squish.h"	// Simon Brown's open source DXT compression library

#include "squishblock.h"

#include <memory.h>
#include <unistd.h>

char squishing[16*4];
int nsquish;

/* C-callable interface to Simon Brown's "squish" library for S3TC DXT1 texture compression */
void squishblock( unsigned char *out, int outstride, unsigned char *in, int instride )
{
    squish::u8 pixels[16*4];
    squish::u8 block[8];

    int i;

    for(i = 0; i < 4; i++)
	memcpy( &pixels[i*4*4 + 0], in + i*instride, 4*4 );

    memcpy( squishing, pixels, sizeof(squishing) ); nsquish++;  // XXX DEBUG

    //squish::Compress( pixels, block, squish::kDxt1 | squish::kColourRangeFit);
    squish::Compress( pixels, block, squish::kDxt1 | squish::kColourRangeFit);

    memcpy( out, block, 8 );
}
