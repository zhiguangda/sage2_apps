#!/bin/sh

make -j8 cefsimple

# folder with:
# cef.pak  chrome-sandbox  devtools_resources.pak  libcef.so  libffmpegsumo.so locales

target=linux

mv cefsimple  $target

sudo chown root:root "$target"/chrome-sandbox
sudo chmod 4755 "$target"/chrome-sandbox
