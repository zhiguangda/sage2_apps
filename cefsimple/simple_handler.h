// Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.

#ifndef CEF_TESTS_CEFSIMPLE_SIMPLE_HANDLER_H_
#define CEF_TESTS_CEFSIMPLE_SIMPLE_HANDLER_H_

#include "include/cef_client.h"

#include <jpeglib.h>
#include <stdint.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XF86keysym.h>

#include <list>

class SimpleHandler : public CefClient,
                      public CefDisplayHandler,
                      public CefLifeSpanHandler,
                      public CefRenderHandler,
                      public CefLoadHandler {
 public:
  SimpleHandler(int, int);
  ~SimpleHandler();

  // Provide access to the single global instance of this object.
  static SimpleHandler* GetInstance();

  // CefClient methods:
  virtual CefRefPtr<CefDisplayHandler> GetDisplayHandler() OVERRIDE {
    return this;
  }
  virtual CefRefPtr<CefLifeSpanHandler> GetLifeSpanHandler() OVERRIDE {
    return this;
  }
  virtual CefRefPtr<CefLoadHandler> GetLoadHandler() OVERRIDE {
    return this;
  }
  virtual CefRefPtr<CefRenderHandler> GetRenderHandler() {
    return this;
  }

  void SetRenderSize(CefRefPtr<CefBrowser> browser, int width, int height);
  bool GetViewRect(CefRefPtr<CefBrowser> browser, CefRect &rect);
  void OnPaint(CefRefPtr<CefBrowser> browser, PaintElementType type, const RectList &dirtyRects, const void *buffer, int width, int height);
  unsigned char* BGRA_to_RGB(unsigned char* bgra, int width, int height);
  unsigned char* BGRA_to_JPEG(unsigned char* bgra, int width, int height, int quality, unsigned long* arr_length);
  char* base64_encode(unsigned char* bin, unsigned long input_length, unsigned long *output_length);

  std::string GetJPEGFrame(CefRefPtr<CefBrowser> browser);
  void PointerMove(CefRefPtr<CefBrowser> browser, int mouseX, int mouseY);
  void PointerPress(CefRefPtr<CefBrowser> browser, int mouseX, int mouseY, std::string button);
  void PointerRelease(CefRefPtr<CefBrowser> browser, int mouseX, int mouseY, std::string button);
  void PointerScroll(CefRefPtr<CefBrowser> browser, int mouseX, int mouseY, int deltaX, int deltaY);
  void SpecialKey(CefRefPtr<CefBrowser>browser, int charCode, std::string state);
  void Keyboard(CefRefPtr<CefBrowser>browser, int charCode);
    

  // Default sizes
  int GetDefaultWidth()  { return m_width;};
  int GetDefaultHeight() { return m_height;};

  // CefDisplayHandler methods:
  virtual void OnTitleChange(CefRefPtr<CefBrowser> browser,
                             const CefString& title) OVERRIDE;

  // CefLifeSpanHandler methods:
  virtual void OnAfterCreated(CefRefPtr<CefBrowser> browser) OVERRIDE;
  virtual bool DoClose(CefRefPtr<CefBrowser> browser) OVERRIDE;
  virtual void OnBeforeClose(CefRefPtr<CefBrowser> browser) OVERRIDE;

  // CefLoadHandler methods:
  virtual void OnLoadError(CefRefPtr<CefBrowser> browser,
                           CefRefPtr<CefFrame> frame,
                           ErrorCode errorCode,
                           const CefString& errorText,
                           const CefString& failedUrl) OVERRIDE;

  // Request that all existing browser windows close.
  void CloseAllBrowsers(bool force_close);

  bool IsClosing() const { return is_closing_; }

private:
    int m_width;
    int m_height;
    int m_browserIdx;

    unsigned int   JavaScriptCodeToNativeCode(unsigned short JSCode);
    unsigned short JavaScriptCodeToUnmodifiedCharCode(unsigned short JSCode);

  // List of existing browser windows. Only accessed on the CEF UI thread.
  typedef std::list<CefRefPtr<CefBrowser> > BrowserList;
  BrowserList browser_list_;

  typedef struct BrowserInfo_t { int id;int width;int height;std::string jpegFrame;} BrowserInfo;
  std::vector<BrowserInfo> browsersInfos;

  bool is_closing_;

  // Include the default reference counting implementation.
  IMPLEMENT_REFCOUNTING(SimpleHandler);
};

#endif  // CEF_TESTS_CEFSIMPLE_SIMPLE_HANDLER_H_
