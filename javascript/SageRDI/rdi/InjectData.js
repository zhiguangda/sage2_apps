/**
 * RiverDisasterInfo -- Provides information from http://www.river.go.jp in a SAGE friendly way
 *
 * Written by Matthew Ready based on the WebView app.
 *
 * This script is injected into a page with a map to provide the dashed line overlay.
 *
 * Example page:
 * http://www.river.go.jp/kawabou/ipGaikyoMap.do?areaCd=82&prefCd=0201&townCd=&gamenId=01-0704&fldCtlParty=no
 */
"use strict";

(function() {
    var typeOfDataElement = document.querySelector(".tableDummy table:nth-child(1) tr:nth-child(2)");
    var topDiv = document.querySelector("div#top");
    var dataDiv = document.querySelector("div.kobetuCntt");
    if (typeOfDataElement && topDiv && dataDiv) {
        var typeOfText = typeOfDataElement.innerText;
        topDiv.remove();

        var formElement = document.querySelector("body > form");
        if (formElement.innerText) {
            dataDiv.style.top = "80px";
            formElement.style.position = "relative";
            formElement.style.top = "-55px";
        } else {
            dataDiv.style.top = "35px";
        }

        var textNode = document.createElement("div");
        textNode.innerText = typeOfText;
        textNode.setAttribute("style", "font-size: 25px; left: 5px; top: 5px; position: absolute;");
        document.body.insertBefore(textNode, document.body.firstChild);

        // Remove annoying "Compiled by FRICS" logo
        var annoyingLogo = document.querySelectorAll("img");
        for (var i = 0; i < annoyingLogo.length; i++) {
            if (annoyingLogo[i].src.indexOf('/pcstatic/image/company_logo_ip.png') >= 0) {
                annoyingLogo[i].parentElement.remove();
            }
        }
    }
})();
