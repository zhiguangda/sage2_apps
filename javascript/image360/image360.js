// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2015


//
// SAGE2 application: image360
// by: Luc Renambot <renambot@gmail.com>
//
// Copyright (c) 2015
//
// based on the THREE.js demo:
// http://mrdoob.github.io/three.js/examples/webgl_panorama_equirectangular.html
//
//

"use strict";

/* global THREE */

var image360 = SAGE2_WebGLApp.extend({
	init: function(data) {
		this.WebGLAppInit('canvas', data);
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "continuous";

		// SAGE2 Application Settings
		this.renderer = null;
		this.camera   = null;
		this.scene    = null;
		this.ready    = null;

		this.width    = this.element.clientWidth;
		this.height   = this.element.clientHeight;
		this.ready    = false;

		this.isUserInteracting = false;
		this.isSpinning        = true;
		this.onMouseDownMouseX = 0;
		this.onMouseDownMouseY = 0;
		this.lon = 0;
		this.lat = 0;
		this.onMouseDownLon = 0;
		this.onMouseDownLat = 0;
		this.onPointerDownPointerX = 0;
		this.onPointerDownPointerY = 0;
		this.onPointerDownLon = 0;
		this.onPointerDownLat = 0;
		this.phi   = 0;
		this.theta = 0;

		// build the app
		this.initialize(data.date);

		this.resizeCanvas();
		this.refresh(data.date);
	},

	initialize: function(date) {
		// CAMERA
		this.camera = new THREE.PerspectiveCamera(70, this.width / this.height, 1, 1100);
		this.camera.target = new THREE.Vector3(0, 0, 0);
		// SCENE
		this.scene = new THREE.Scene();

		var geometry = new THREE.SphereGeometry(500, 60, 40);
		geometry.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));
		// instantiate a texture loader
		var loader   = new THREE.TextureLoader();
		var material = new THREE.MeshBasicMaterial({
			// map: loader.load(this.resrcPath + 'test.jpg')
			map: loader.load(this.state.file)
		});
		var mesh = new THREE.Mesh(geometry, material);
		this.scene.add(mesh);

		// RENDERER
		if (this.renderer == null) {
			this.renderer = new THREE.WebGLRenderer({
				canvas: this.canvas,
				antialias: true
			});
			this.renderer.setSize(this.width, this.height);
			this.renderer.setPixelRatio(window.devicePixelRatio);
			this.renderer.autoClear = false;

			this.renderer.gammaInput  = true;
			this.renderer.gammaOutput = true;
		}

		this.ready = true;

		// Control the frame rate for an animation application
		this.maxFPS = 20.0;
		// Not adding controls but making the default buttons available
		this.controls.finishedAddingControls();
		this.enableControls = true;

		this.resize(date);
	},

	load: function(date) {
		this.refresh(date);
	},

	draw: function(date) {
		if (this.ready) {
			this.renderer.clear();

			// Auto rotate the cube
			if (this.isSpinning) {
				this.lon += 0.3;
			}

			// update the orientation based on mouse movement
			this.lat   = Math.max(-85, Math.min(85, this.lat));
			this.phi   = THREE.Math.degToRad(90 - this.lat);
			this.theta = THREE.Math.degToRad(this.lon);
			this.camera.target.x = 50 * Math.sin(this.phi) * Math.cos(this.theta);
			this.camera.target.y = 50 * Math.cos(this.phi);
			this.camera.target.z = 50 * Math.sin(this.phi) * Math.sin(this.theta);
			this.camera.lookAt(this.camera.target);

			// distortion
			this.camera.position.copy(this.camera.target).negate();

			this.renderer.render(this.scene, this.camera);
		}
	},

	// Local Threejs specific resize calls.
	resizeApp: function(resizeData) {
		if (this.renderer != null && this.camera != null) {
			this.renderer.setSize(this.canvas.width, this.canvas.height);

			this.camera.setViewOffset(this.sage2_width, this.sage2_height,
					resizeData.leftViewOffset, resizeData.topViewOffset,
					resizeData.localWidth, resizeData.localHeight);
		}
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			this.isUserInteracting = true;
			this.onPointerDownPointerX = position.x;
			this.onPointerDownPointerY = position.y;
			this.onPointerDownLon = this.lon;
			this.onPointerDownLat = this.lat;
		} else if (eventType === "pointerMove" && this.isUserInteracting) {
			this.lon = (this.onPointerDownPointerX - position.x) * 0.5 + this.onPointerDownLon;
			this.lat = (position.y - this.onPointerDownPointerY) * 0.5 + this.onPointerDownLat;
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			this.isUserInteracting = false;
		} else if (eventType === "pointerScroll") {
			this.camera.fov -= data.wheelDelta * 0.04;
			if (this.camera.fov < 0.1) {
				this.camera.fov = 0.1;
			} else if (this.camera.fov > 179.80) {
				this.camera.fov = 179.80;
			}
			this.camera.updateProjectionMatrix();
		} else if (eventType === "widgetEvent") {
			// widget
		} else if (eventType === "keyboard") {
			if (data.character === "r" || data.character === "R") {
				// Reset all parameters
				this.isUserInteracting = false;
				this.onMouseDownMouseX = 0;
				this.onMouseDownMouseY = 0;
				this.lon = 0;
				this.lat = 0;
				this.onMouseDownLon = 0;
				this.onMouseDownLat = 0;
				this.onPointerDownPointerX = 0;
				this.onPointerDownPointerY = 0;
				this.onPointerDownLon = 0;
				this.onPointerDownLat = 0;
				this.phi   = 0;
				this.theta = 0;
				this.camera.fov = 70;
			} else if (data.character === " ") {
				this.isSpinning = !this.isSpinning;
			}
		} else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") { // left
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") { // up
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") { // right
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") { // down
				this.refresh(date);
			}
		}
	}
});
