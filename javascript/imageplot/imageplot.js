//
// SAGE2 application: imageplot
// by: Dylan Kobayashi <dylank@hawaii.edu>
//
// Copyright (c) 2018
//

"use strict";

/*
How to use:


*/


var imageplot = sage2_webview_appCoreV01_extendWebview({
	webpageAppSettings: {
		setSageAppBackgroundColor: true, // Web pages without background values will be transparent.
		backgroundColor: "white", // Used if above is true, can also use rgb and hex strings
		enableRightClickNewWindow: false, // right clicking on images or links open new webview
		printConsoleOutputFromPage: true, // If true, when page prints to command line, app will print in display client

		// The following will include the default Webview context menu entry if set to true.
		enableUiContextMenuEntries: {
			navigateBack:       false, // alt left-arrow
			navigateForward:    false, // alt right-arrow
			reload:             true, // alt r
			autoRefresh:        false, // must be selected from UI context menu
			consoleViewToggle:  false, // must be selected from UI context menu
			zoomIn:             true, // alt up-arrow
			zoomOut:            true, // alt down-arrow
			urlTyping:          false, // must be typed from UI context menu
			copyUrlToClipboard: false, // must be typed from UI context menu
		},
	},
	init: function(data) {
		// Will be called after initial SAGE2 init()
		// this.element will refer to the webview tag
		this.resizeEvents = "continuous"; // Recommended not to change. Options: never, continuous, onfinish

		// Path / URL of the page you want to show
		this.changeURL(this.resrcPath + "/webpage/index.html", false);
	},
	load: function(date) {
		// OPTIONAL
		// The state will be automatically passed to your webpage through the handler you gave to SAGE2_AppState
		// Use this if you want to alter the state BEFORE it is passed to your webpage. Access with this.state
	},
	draw: function(date) {
		// OPTIONAL
		// Your webpage will be in charge of its view
		// Use this if you want to so something within the SAGE2 Display variables
		// Be sure to set 'this.maxFPS' within init() if this is desired.
		// FPS only works if instructions sets animation true
		if(this.isCollectingImages) {
			this.imageCollectionCheck();
		}
	},
	resize: function() {
		// OPTIONAL
	},
	getContextEntries: function() {
		// OPTIONAL
		// This can be used to allow UI interaction to your webpage
		// Entires are added after entries of enableUiContextMenuEntries 
		var entries = [];
		var entry;
		
		
		if (this.isCollectingImages) {
			entry = {};
			entry.description = "Stop Image Collection";
			entry.callback = "stopImageCollection";
			entry.parameters = {};
			entries.push(entry);
		} else {
			entry = {};
			entry.description = "Start Image Collection";
			entry.callback = "startImageCollection";
			entry.parameters = {};
			entries.push(entry);
		}


		entry = {};
		entry.description = "Next Image";
		entry.accelerator = "\u2192 or \u2193";     // ALT up-arrow
		entry.callback = "changeImage";
		entry.parameters = {};
		entry.parameters.dir = "Right";
		entries.push(entry);

		entry = {};
		entry.description = "Previous Image";
		entry.accelerator = "\u2190 or \u2191";     // ALT down-arrow
		entry.callback = "changeImage";
		entry.parameters = {};
		entry.parameters.dir = "Left";
		entries.push(entry);

		entries.push({description: "separator"});

		entry = {};
		entry.description = "Map Zoom In";
		entry.callback = "zoomMap";
		entry.parameters = {};
		entry.parameters.dir = "zoomin";
		entries.push(entry);

		entry = {};
		entry.description = "Map Zoom Out";
		entry.callback = "zoomMap";
		entry.parameters = {};
		entry.parameters.dir = "zoomout";
		entries.push(entry);

		entries.push({description: "separator"});




		return entries;
	},

	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// Add optional functions

	startImageCollection: function() {
		this.isCollectingImages = true;
		this.getFullContextMenuAndUpdate();
		this.maxFPS = 5;
		this.updateTitle("Image Plot - COLLECTING DATA");
	},
	stopImageCollection: function() {
		this.isCollectingImages = false;
		this.getFullContextMenuAndUpdate();
		this.updateTitle("Image Plot");
	},

	imageCollectionCheck: function() {
		// Check if has list of images already
		if (!this.collectedImages) {
			this.collectedImages = [];
		}
		let keys = Object.keys(applications);
		let app, lat, lng, date, thumbnailPath;
		let addedOne = false;
		// go through each application
		for (let i = 0; i < keys.length; i++) {
			app = applications[keys[i]];
			if (app.application === "image_viewer") {
				if (app.state.exif && app.state.exif.GPSLatitude && app.state.exif.GPSLongitude) {
					lat = this.convertDegMinSecDirToSignedDegree(app.state.exif.GPSLatitude);
					lng = this.convertDegMinSecDirToSignedDegree(app.state.exif.GPSLongitude);
					thumbnailPath = this.getPathOfThumbnail(app.state.img_url);
					date = app.state.exif.DateTimeOriginal;
					if (this.collectedImages.indexOf(thumbnailPath) === -1) {
						this.collectedImages.push(thumbnailPath);
						this.state.images.push({path: thumbnailPath, date, geolocation: {lat, lng}});
					}
					app.close();
					addedOne = true;
				}
			}
		}
		if (addedOne) {
			this.load();
		}
	},
	/**
	 * Converts string with degree, minute, second, direction to signed degree.
	 * To avoid being token specific, checks for numbers rather than symbols.
	 * Example coordinates from some images:
	 *
	 * lat: "21 deg 41' 33.14\" N",
	 * lng: "157 deg 50' 55.48\" W"
	 * lat: "19 deg 48' 59.66\" N ",
	 * lng:"156 deg 10' 45.01\" W "
	 * lat:"21 deg 52' 26.86\" N "
	 * lng:"159 deg 27' 22.96\" W "
	 *
	 * @method convertDegMinSecDirToSignedDegree
	 * @param {String} input - Will convert degree with mins and seconds notation to signed degree.
	 */
	convertDegMinSecDirToSignedDegree: function (input) {
		// find the number first, might be prefix fluff
		var index = 0;
		var partIndex = -1;
		var findingNextNumber = true;
		// deg, min, sec, dir
		var parts = ["", "", "", ""];
		// for each of the characters
		while (index < input.length) {
			// if finding next number
			if (findingNextNumber) {
				// if finding next number, but have gone through first 3, want direction
				if (partIndex == 2) {
					if (input.charAt(index) === "N"
						|| input.charAt(index) === "S"
						|| input.charAt(index) === "E"
						|| input.charAt(index) === "W") {
						parts[3] += input.charAt(index);
					}
				} else if (!isNaN(input.charAt(index))) {
					// else if this char is a number moveup part index and stop looking
					partIndex++;
					findingNextNumber = false;
				}
			} // if at the next number, add to the part
			if (!findingNextNumber) {
				// if it is a number or a decimal, add it.
				if (!isNaN(input.charAt(index))
					|| input.charAt(index) === ".") {
					parts[partIndex] += input.charAt(index);
				} else {
					// hitting a non-number or . means in between numbers
					findingNextNumber = true;
				}
			} // always increase the index
			index++;
		}
		// for all but the direction, convert to floats
		for (let i = 0; i < parts.length - 1; i++) {
			parts[i] = parseFloat(parts[i]);
		}
		var justDegrees = parts[0];
		justDegrees += parts[1] / 60;
		justDegrees += parts[2] / (60 * 60);

		// flip the sign depending on which direction the count was in
		if (parts[3] == "S" || parts[3] == "W") {
			justDegrees = justDegrees * -1;
		}
		return justDegrees;
	},

	getPathOfThumbnail: function(originalImagePath) {
		// Chop to just name
		while (originalImagePath.indexOf("/") !== -1) {
			originalImagePath = originalImagePath.substring(originalImagePath.indexOf("/") + 1);
		}
		while (originalImagePath.indexOf("\\") !== -1) {
			originalImagePath = originalImagePath.substring(originalImagePath.indexOf("\\") + 1);
		}

		// Start with this app's path
		let path = this.resrcPath;
		path = path.substring(0, path.indexOf("/user/"));
		path += "/user/assets/" + originalImagePath + "_512.jpg";

		return path;
	},



	changeImage: function(responseObject) {
		let code = responseObject.dir;
		this.element.sendInputEvent({
			type: "keyDown",
			keyCode: code,
			modifiers: null
		});
	},

	zoomMap: function(responseObject) {
		if (this.isElectron()) {
			var dir = responseObject.dir;
			if (dir === "zoomin") {
				this.element.executeJavaScript("mapZoom('zoomin')");
			} else if (dir === "zoomout") {
				this.element.executeJavaScript("mapZoom('zoomout')");
			}
		}
	},




});