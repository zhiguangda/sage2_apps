// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

"use strict";

var noteAge = SAGE2_App.extend({

	init: function(data) {
		// SAGE2_App
		this.SAGE2Init("div", data); // call super-class 'init'
		this.element.id = "div" + this.id; // Set id of element
		this.maxFPS = 2.0; // FPS only works if instructions sets animation true
		this.resizeEvents = "never"; // Options: never, continuous, onfinish
		this.passSAGE2PointerAsMouseEvents = false; // Set to true to enable SAGE2 auto conversion of pointer events to mouse events

		//
		this.appSpecific();
	},

	//
	appSpecific: function() {
		this.debug = false;
		this.element.style.background = "white";
		this.timeBetweenColorUpdates = 300; // ms
		this.arrayOfEditors = [];

		// inject html code    file to grab from,    element.innerHTML to override
		this.loadHtmlFromFile(this.resrcPath + "design.html", this.element, () => {
			if (this.isAnotherOpen()) {
				requestAnimationFrame(() => {this.close();}); // only One at a time
			} else {
				this.postHtmlFillActions();
			}
		});
	},

	debugprint: function() {
		if (this.debug) {
			console.log("DEBUG sessionHelper>", arguments);
		}
	},

	/**
	 * If a design is loaded from html, this would be where to add event listeners. Name must match the function call named in loadHtmlFromFile
	 *
	 * @method     postHtmlFillActions
	 */
	postHtmlFillActions: function() {
		// Page stuff
		this.setupTimers();
	},

	// Use load for view synchronization across multiple clients / remote sites
	load: function(date) {},

	// Draw is called based on the maxFPS value and animation true
	draw: function(date) {},

	// Needs resizeEvents set to continuous or onfinish
	resize: function(date) {},

	/**
	* To enable right click context menu support this function needs to be present.
	*
	* Must return an array of entries. An entry is an object with three properties:
	*	description: what is to be displayed to the viewer.
	*	callback: String containing the name of the function to activate in the app. It must exist.
	*	parameters: an object with specified datafields to be given to the function.
	*		The following attributes will be automatically added by server.
	*			serverDate, on the return back, server will fill this with time object.
	*			clientId, unique identifier (ip and port) for the client that selected entry.
	*			clientName, the name input for their pointer. Note: users are not required to do so.
	*			clientInput, if entry is marked as input, the value will be in this property. See pdf_viewer.js for example.
	*		Further parameters can be added. See pdf_view.js for example.
	*/
	getContextEntries: function() {
		var entries = [];
		
		entries.push({
			description: "Open edit page in new tab (proto)",
			callback: "SAGE2_openPage",
			parameters: {
				url: this.resrcPath + "webpage/index.html?appId=" + this.id + "&pointerName=guest&pointerColor=gray",
			}
		});

		entries.push({
			description: "Backup - Copy URL of controls to clipboard",
			callback: "SAGE2_copyURL",
			parameters: {
				url: this.resrcPath + "webpage/index.html?appId=" + this.id + "&pointerName=guest&pointerColor=gray",
			}
		});

		return entries;
	},

	// Fill this out to handle pointer events. OR use the passSAGE2PointerAsMouseEvents
	event: function(eventType, position, user_id, data, date) {},

	// Is activated before closing, use this for cleanup or saving
	quit: function() {
		if (this.sessionTimers && this.sessionTimers.colorChecker) clearInterval(this.sessionTimers.colorChecker);
	},


	// ------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------
	// Support functions

	isAnotherOpen: function() {
		let appNames = Object.keys(applications);
		for (let i = 0; i < appNames.length; i++) {
			if ((applications[appNames[i]].application == "noteAge") && (appNames[i] != this.id)) {
				return true;
			}
		}
		return false;
	},


	setupTimers: function() {
		this.sessionTimers = {};
		// Probably can use one timer for handling all
		this.debugprint("erase me, setting up interval for colorUpdateLogic");
		this.sessionTimers.colorChecker = setInterval(() => {
			this.colorUpdateLogic();
		}, this.timeBetweenColorUpdates);
	},

	colorUpdateLogic: function() {
		this.debugprint("erase me, sessionBackupLogic");
		let allNoteApps = this.getAllNoteApps();
		this.applyColorToAllNoteApps(allNoteApps);
	},

	getAllNoteApps: function() {
		let notes = [];
		let appIds = Object.keys(applications);
		for (let i = 0; i < appIds.length; i++) {
			if (applications[appIds[i]].application === "quickNote") notes.push(applications[appIds[i]]);
		}
		return notes;
	},

	applyColorToAllNoteApps: function(notes) {
		let timeSinceStart, color;
		for (let i = 0; i < notes.length; i++) {
			timeSinceStart = (Date.now() - notes[i].startDate.getTime()) / 1000; // turn into seconds
			color = this.getColorBasedOnTime(timeSinceStart);
			if (color) notes[i].setColor({color}); // just in case...
		}
	},

	getColorBasedOnTime: function(timeSinceStart) {
		let colorEntries = this.determineTimeEntries(timeSinceStart);
		let colorString;
		let tc;
		if (colorEntries.length > 1) {
			let dTime = colorEntries[1].time - colorEntries[0].time;
			dTime = (timeSinceStart - colorEntries[0].time) / dTime;
			tc = [colorEntries[1].color[0] - colorEntries[0].color[0],
				colorEntries[1].color[1] - colorEntries[0].color[1],
				colorEntries[1].color[2] - colorEntries[0].color[2]];
			// Need to add
			colorString = "rgb(" + parseInt(colorEntries[0].color[0] + (dTime * tc[0]))
				+ "," + parseInt(colorEntries[0].color[1] + (dTime * tc[1]))
				+ "," + parseInt(colorEntries[0].color[2] + (dTime * tc[2])) + ")";
				this.debugprint("erase me, two entries");
		} else if (colorEntries.length == 1) {
			tc = colorEntries[0].color;
			colorString = "rgb(" + parseInt(tc[0]) + "," + parseInt(tc[1]) + "," + parseInt(tc[2]) + ")";
			this.debugprint("erase me, one entry");
		}
		return colorString;
	},

	determineTimeEntries: function(timeSinceStart) {
		let groups = this.state.timeGroups;
		let i = 0;
		for (; i < groups.length; i++) {
			if (timeSinceStart < groups[i].time) {
				break;
			}
		}
		if (i == 0) {
			return [];
		} if (i >= groups.length) {
			return [groups[groups.length - 1]]; // return the last one, it gets set to that color
		}
		return [groups[i - 1], groups[i]];
	},


	// ------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------
	// Client communication functions


	/**
	 * Adds a clientId as an editer. Activates after launch or context menu edit.
	 * Everyone in the array should be able to update this app correctly and receive each other's updates.
	 *
	 * @method addClientIdAsEditor
	 * @param {Object} responseObject - Should contain the following.
	 * @param {Object} responseObject.clientId - Unique id of client given by server.
	 * @param {Object} responseObject.clientName - User input name of pointer.
	*/
	fromControls_addClientIdAsEditor: function(responseObject) {
		if (isMaster) {
			// add the client who responded to the list of editors.
			this.arrayOfEditors.push(responseObject.clientId);
			// send back to client the OK to start editing.
			// sendDataToClient: function(clientDest, func, paramObj) appId is automatically added to param object
			this.sendDataToClient(responseObject.clientId, "fromS2_timeGroups", {timeGroups: this.state.timeGroups});
		}
	},


	fromControl_updateTimeGroups: function(responseObject) {
		this.state.timeGroups = responseObject.timeGroups;
		this.SAGE2Sync(true);
	},




	// ------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------
	// Default Support functions

	/**
	 * This will load the visual layout from html file included in the folder
	 * Done so one doesn't have to programatically generate layout.
	 *
	 * @method     loadHtmlFromFile
	 * @param      {String}  relativePathFromAppFolder From the containing app folder, path to file
	 * @param      {String}  whereToAppend     Node who's innerHTML will be set to content
	 * @param      {String}  callback     What function to call after getting the file
	 */
	loadHtmlFromFile: function(relativePathFromAppFolder, whereToAppend, callback) {
		var _this = this;
		readFile(relativePathFromAppFolder, function(err, data) {
			_this.loadIntoAppendLocation(whereToAppend, data);
			callback();
		}, 'TEXT');
	},

	/**
	 * Called after xhr gets html content
	 * Main thing to note is that id fields are altered to be prefixed with SAGE2 assigned id
	 *
	 * @method     loadIntoAppendLocation
	 * @param      {String}  whereToAppend Node who's innerHTML will be set to content
	 * @param      {String}  responseText     Content of the file
	 */
	loadIntoAppendLocation: function(whereToAppend, responseText) {
		var content = "";
		// id and spaces because people aren't always consistent
		var idIndex;

		// find location of first id div. Because there will potentially be multiple apps.
		idIndex = this.findNextIdInHtml(responseText);

		// for each id, prefix it with this.id
		while (idIndex !== -1) {
			// based on id location move it over
			content += responseText.substring(0, idIndex);
			responseText = responseText.substring(idIndex);
			// collect up to the first double quote. design.html has double quotes, but HTML doesn't require.
			content += responseText.substring(0, responseText.indexOf('"') + 1);
			responseText = responseText.substring(responseText.indexOf('"') + 1);
			// apply id prefix
			content += this.id;
			// collect rest of id
			content += responseText.substring(0, responseText.indexOf('"') + 1);
			responseText = responseText.substring(responseText.indexOf('"') + 1);

			// find location of first id div. Because there will potentially be multiple apps.
			idIndex = this.findNextIdInHtml(responseText);
		}
		content += responseText;
		whereToAppend.innerHTML = content;
	},

	/**
	 * This returns the index of the first location of id
	 * Accounts for 0 to 3 spaces between id and =
	 *
	 * @method     findNextIdInHtml
	 */
	findNextIdInHtml: function(responseText) {
		// find location of first id div. Because there will potentially be multiple apps.
		// the multiple checks are incase writers are not consistent
		var idIndex = responseText.indexOf("id=");
		var ids1 = responseText.indexOf("id =");
		var ids2 = responseText.indexOf("id  =");
		var ids3 = responseText.indexOf("id   =");
		// if (idIndex isn't found) or (is found but ids1 also found and smaller than idIndex)
		if ((idIndex === -1) || (ids1 > -1 && ids1 < idIndex)) {
			idIndex = ids1;
		}
		if ((idIndex === -1) || (ids2 > -1 && ids2 < idIndex)) {
			idIndex = ids2;
		}
		if ((idIndex === -1) || (ids3 > -1 && ids3 < idIndex)) {
			idIndex = ids3;
		}
		return idIndex;
	},

});
