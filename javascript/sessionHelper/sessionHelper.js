// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

"use strict";

var sessionHelper = SAGE2_App.extend({

	init: function(data) {
		// SAGE2_App
		this.SAGE2Init("div", data); // call super-class 'init'
		this.element.id = "div" + this.id; // Set id of element
		this.maxFPS = 2.0; // FPS only works if instructions sets animation true
		this.resizeEvents = "never"; // Options: never, continuous, onfinish
		this.passSAGE2PointerAsMouseEvents = false; // Set to true to enable SAGE2 auto conversion of pointer events to mouse events

		//
		this.appSpecific();
	},

	//
	appSpecific: function() {
		this.debug = false;
		this.noteFileContainingPersistentDataName = "sessionHelperData.note";
		this.noteFileContainingPersistentDataFullPath = "/user/notes/" + this.noteFileContainingPersistentDataName;
		this.doneWithDataFileLoadPhase = false;
		this.arrayOfEditors = [];
		this.shouldLoadTestingData = false;
		this.timeBetweenBackups = 300000; // ms. 10s is 10000; 5mins is 300000
		this.allowIncrementalBackups = false; // False: only write to one file entry every ~5 minutes
		this.element.style.background = "white";
		this.element.style.fontSize = ui.titleTextSize * 2 + "px";

		// inject html code    file to grab from,    element.innerHTML to override
		this.loadHtmlFromFile(this.resrcPath + "design.html", this.element, () => {
			if (this.isAnotherOpen()) {
				requestAnimationFrame(() => {this.close();});
			} else {
				this.postHtmlFillActions();
			}
		});
	},

	debugprint: function() {
		if (this.debug) {
			console.log("DEBUG sessionHelper>", arguments);
		}
	},

	/**
	 * If a design is loaded from html, this would be where to add event listeners. Name must match the function call named in loadHtmlFromFile
	 *
	 * @method     postHtmlFillActions
	 */
	postHtmlFillActions: function() {
		// Page stuff
		this.currentSessionDiv = document.getElementById(this.id + "currentSessionDiv");
		this.otherSessionsAvailableDiv = document.getElementById(this.id + "otherSessionsAvailableDiv");


		// App stuff
		this.tryLoadNoteData(); // will trigger callback after result
	},

	// Use load for view synchronization across multiple clients / remote sites
	load: function(date) {},

	// Draw is called based on the maxFPS value and animation true
	draw: function(date) {},

	// Needs resizeEvents set to continuous or onfinish
	resize: function(date) {},

	/**
	* To enable right click context menu support this function needs to be present.
	*
	* Must return an array of entries. An entry is an object with three properties:
	*	description: what is to be displayed to the viewer.
	*	callback: String containing the name of the function to activate in the app. It must exist.
	*	parameters: an object with specified datafields to be given to the function.
	*		The following attributes will be automatically added by server.
	*			serverDate, on the return back, server will fill this with time object.
	*			clientId, unique identifier (ip and port) for the client that selected entry.
	*			clientName, the name input for their pointer. Note: users are not required to do so.
	*			clientInput, if entry is marked as input, the value will be in this property. See pdf_viewer.js for example.
	*		Further parameters can be added. See pdf_view.js for example.
	*/
	getContextEntries: function() {
		var entries = [];
		
		entries.push({
			description: "Open edit page in new tab (proto)",
			callback: "SAGE2_openPage",
			parameters: {
				url: this.resrcPath + "webpage/index.html?appId=" + this.id + "&pointerName=guest&pointerColor=gray",
			}
		});

		entries.push({
			description: "Backup - Copy URL of controls to clipboard",
			callback: "SAGE2_copyURL",
			parameters: {
				url: this.resrcPath + "webpage/index.html?appId=" + this.id + "&pointerName=guest&pointerColor=gray",
			}
		});

		entries.push({ description: "separator" });

		entries.push({
			description: "Set the session name:",
			callback: "setTheSessionName",
			inputField: true,
			inputFieldSize: 20,
			parameters: {},
		});

		return entries;
	},

	// Fill this out to handle pointer events. OR use the passSAGE2PointerAsMouseEvents
	event: function(eventType, position, user_id, data, date) {},

	// Is activated before closing, use this for cleanup or saving
	quit: function() {
		if (this.sessionTimers && this.sessionTimers.min5) clearInterval(this.sessionTimers.min5);
	},


	// ------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------
	// Support functions

	isAnotherOpen: function() {
		let appNames = Object.keys(applications);
		for (let i = 0; i < appNames.length; i++) {
			if ((applications[appNames[i]].application == "sessionHelper") && (appNames[i] != this.id)) {
				return true;
			}
		}
		return false;
	},

	tryLoadNoteData: function() {
		readFile(this.noteFileContainingPersistentDataFullPath, (err, data) => {
			// If problem with reading file, assume corrupted or doesn't exist.
			if (err) {
				this.state.sessionList = [];
				this.debugprint("erase me, could not find note, session list is currently empty");
			} else { // read, text is in data
				try {
					// trim off the first two lines(?)
					let lines = data.split("\n");
					let obj = JSON.parse(lines[2]); // name, color, data
					this.state.sessionList = obj;
					this.debugprint("erase me, node data", obj);
				} catch (e) {
					this.state.sessionList = [];
					this.debugprint("erase me, error during parse, session list is currently empty");
				}
			}
			this.doneWithDataFileLoadPhase = true;
			this.setupTimers();
			if (this.shouldLoadTestingData) {
				this.debugprint("erase me, loading test data");
				this.loadTestingData();
			}
			this.updateOtherSessionsAvailableDiv();
			if (this.state.currentSessionName) {
				this.updateTheDisplayedSessionNameOnApp();
			}
		}, 'TEXT');
	},

	loadTestingData: function() {

		this.debugprint("erase me, what is session list like?", this.state.sessionList);
		if (this.state.sessionList) {
			this.state.sessionList.push({
				"name": "test01",
				"history": [Date.now()]
			});
			this.state.sessionList.push({
				"name": "test02",
				"history": [Date.now()]
			});

			this.debugprint("erase me, loaded testing data");
		}
	},

	savePersistentDataFile: function() {
		var fileData = {};
		fileData.fileType = "note"; // Extension
		fileData.fileName = this.noteFileContainingPersistentDataName; // Fullname with extension
		// What to save in the file, uses format of the quickNote
		fileData.fileContent = Date.now() + "\n" + "yellow" + "\n";
		fileData.fileContent += JSON.stringify(this.state.sessionList);
		wsio.emit("saveDataOnServer", fileData);
	},

	setupTimers: function() {
		// Only do tracking on master
		if (isMaster) {
			this.sessionTimers = {};
			// Probably can use one timer for handling all
			this.debugprint("erase me, setting up interval for sessionBackupLogic");
			this.sessionTimers.min5 = setInterval(() => {
				this.sessionBackupLogic();
			}, this.timeBetweenBackups);
		}
	},

	// Activates roughly 5 minutes
	sessionBackupLogic: function() {
		this.debugprint("erase me, sessionBackupLogic");
		if (!isMaster) return; // Only master should perform saves
		if (this.state.currentSessionName) { // if there is a current session
			let sessionData = this.getSessionDataGivenName(this.state.currentSessionName);
			let history = sessionData.history;
			if (sessionData) {
				let timeNow = Date.now();
				let timePieces = {};
				timePieces.min15 = 1000 * 60 * 15; // ms
				// timePieces.min30 = 1000 * 60 * 15; // ms
				// timePieces.hour1 = 1000 * 60 * 15; // ms
				// timePieces.hour2 = 1000 * 60 * 15; // ms
				// timePieces.hour3 = 1000 * 60 * 15; // ms
				// timePieces.hour4 = 1000 * 60 * 15; // ms
				// timePieces.day1 = 1000 * 60 * 15; // ms
				if (history > 0) {
					// If there are at least two history entries, and 2nd from last is greater than 15 mins
					if ((history.length > 1) && ((timeNow - history[history.length - 1]) > timePieces.min15) && this.allowIncrementalBackups) {
						// Make a new entry
						// Currently disabled
						console.log("Incremental logging is not implemented yet");
					}
				} else {

					this.debugprint("erase me, adding to blank history");
					history.push(Date.now()); // if no history, always make
				}
				// Save session
				this.debugprint("erase me, trying to save session");
				this.saveCurrentSession();
			}
		}
	},

	getSessionDataGivenName: function(nameOfSession) {
		for (let i = 0; i < this.state.sessionList.length; i++) {
			if (this.state.sessionList[i].name == nameOfSession) {
				return this.state.sessionList[i];
			}
		}
		return null;
	},


	setTheSessionName: function(responseObject) {
		responseObject.clientInput = responseObject.clientInput.trim(); // remove leading, trailling whitespace
		if (responseObject.clientInput.length < 2) return; // sessions shouldn't be 1 character?
		
		let data = this.getSessionDataGivenName(responseObject.clientInput);
		if (!data) {
			this.makeNewSession(responseObject.clientInput);
		}
		this.switchToSessionName(responseObject.clientInput, responseObject.clientId);
	},

	updateTheDisplayedSessionNameOnApp: function() {
		this.currentSessionDiv.innerHTML = "Current Session:<br>" + this.state.currentSessionName;
	},

	// Should be called by setTheSessionName
	makeNewSession: function(name) {
		this.state.sessionList.push({
			"name": name,
			"history": [Date.now()]
		});

		this.notifyAllClientsOfNewSessionName(name);

		this.updateOtherSessionsAvailableDiv();
	},

	updateOtherSessionsAvailableDiv: function() {

		let htmlContent = "Other Sessions Available:"
		for (let i = 0; i < this.state.sessionList.length; i++) {
			htmlContent += "\n<br>" + this.state.sessionList[i].name;
		}

		this.otherSessionsAvailableDiv.innerHTML = htmlContent;
	},

	getSessionNames: function() {
		let names = [];
		for (let i = 0; i < this.state.sessionList.length; i++) {
			names.push(this.state.sessionList[i].name);
		}
		return names;
	},

	switchToSessionName: function(nameOfSessionToSwitchTo, clientId) {
		// Don't switch if the session to switch to is already being shown
		if (this.state.currentSessionName && (this.state.currentSessionName == nameOfSessionToSwitchTo)) return;
		// Can only switch it if exists
		let names = this.getSessionNames();
		if (names.includes(nameOfSessionToSwitchTo)) {
			this.saveCurrentSession();
			this.closeAllOtherApps();
			this.openSessionWithName(nameOfSessionToSwitchTo, clientId);
			this.notifyAllClientsOfSessionSwitchedTo(nameOfSessionToSwitchTo);
			this.state.currentSessionName = nameOfSessionToSwitchTo;

			// Update the session name being displayed
			this.updateTheDisplayedSessionNameOnApp();
			// Relay information back to server
			this.SAGE2Sync();
		}
	},

	saveCurrentSession: function() {
		if (this.state.currentSessionName) {
			this.debugprint("erase me, saveCurrentSession. 1) save the session based on last history entry, 2) write to the note file");
			let sessionData = this.getSessionDataGivenName(this.state.currentSessionName);
			let saveName = sessionData.name + " " + sessionData.history[sessionData.history.length - 1];
			this.debugprint("erase me, saveCurrentSession. saving with name: " + saveName);
			wsio.emit("saveSession", saveName);
			this.savePersistentDataFile();
		}
	},

	closeAllOtherApps: function() {
		let appNames = Object.keys(applications);
		for (let i = 0; i < appNames.length; i++) {
			if ((appNames[i] != this.id)) {
				applications[appNames[i]].close();
			}
		}
	},

	openSessionWithName: function(name, clientId) {
		let sessionData = this.getSessionDataGivenName(name);
		let saveName = sessionData.name + " " + sessionData.history[sessionData.history.length - 1];
		wsio.emit("loadFileFromServer", {
			application: "load_session",
			filename: saveName,
			user: clientId // client id is necessary to pass the userlist.isAllowed check
		});
	},



	// ------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------
	// Client communication functions


	/**
	 * Adds a clientId as an editer. Activates after launch or context menu edit.
	 * Everyone in the array should be able to update this app correctly and receive each other's updates.
	 *
	 * @method addClientIdAsEditor
	 * @param {Object} responseObject - Should contain the following.
	 * @param {Object} responseObject.clientId - Unique id of client given by server.
	 * @param {Object} responseObject.clientName - User input name of pointer.
	*/
	addClientIdAsEditor: function(responseObject) {
		if (isMaster) {
			// add the client who responded to the list of editors.
			this.arrayOfEditors.push(responseObject.clientId);
			// send back to client the OK to start editing.
			var dataForClient = {};
			dataForClient.nameList = this.getSessionNames();
			// sendDataToClient: function(clientDest, func, paramObj) appId is automatically added to param object
			this.sendDataToClient(responseObject.clientId, "fromS2_sessionNameList", dataForClient);
			// If there is a current name, send it
			if (this.state.currentSessionName) this.sendDataToClient(responseObject.clientId, "fromS2_switchedToSession", {"sessionName": this.state.currentSessionName});
		}
	},



	// These will come from client activations
	fromControls_handleNewSessionRequest: function(responseObject) {
		// clean
		responseObject.newSessionName = responseObject.newSessionName.trim();
		// get session names
		let data = this.getSessionDataGivenName(responseObject.newSessionName);
		// if no in there, then try make
		if (!data) {
			this.makeNewSession(responseObject.newSessionName);
		}
	},

	notifyAllClientsOfNewSessionName: function(name) {
		if (isMaster) {
			for (let i = 0; i < this.arrayOfEditors.length; i++) {
				this.sendDataToClient(this.arrayOfEditors[i], "fromS2_newSessionName", {"newSessionName": name});
			}
		}
	},

	fromControls_switchToSessionNameRequest: function (responseObject) {
		responseObject.sessionName = responseObject.sessionName.trim();
		this.switchToSessionName(responseObject.sessionName, responseObject.clientId);
	},

	fromControls_removeSession:  function (responseObject) {
		let nameToRemove = responseObject.sessionName;
		let removed = false;
		// First remove
		for (let i = 0; i < this.state.sessionList.length; i++) {
			if (this.state.sessionList[i].name == nameToRemove) {
				// splice it
				this.state.sessionList.splice(i, 1);
				i--;
				removed = true;
			}
		}
		if (removed) { // Only update if needed
			this.notifyAllClientsOfCurrentSessionList();
			this.updateOtherSessionsAvailableDiv();
			this.SAGE2Sync();
			this.savePersistentDataFile();
		}
	},



	notifyAllClientsOfSessionSwitchedTo: function(nameOfSessionToSwitchTo) {
		if (isMaster) {
			for (let i = 0; i < this.arrayOfEditors.length; i++) {
				this.sendDataToClient(this.arrayOfEditors[i], "fromS2_switchedToSession", {"sessionName": nameOfSessionToSwitchTo});
			}
		}
	},

	// Proably because a remove happened.
	notifyAllClientsOfCurrentSessionList: function() {
		if (isMaster) {
			for (let i = 0; i < this.arrayOfEditors.length; i++) {
				this.sendDataToClient(this.arrayOfEditors[i], "fromS2_sessionNameList", {"nameList": this.getSessionNames()});
			}
		}
	},





	// ------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------
	// Default Support functions

	/**
	 * This will load the visual layout from html file included in the folder
	 * Done so one doesn't have to programatically generate layout.
	 *
	 * @method     loadHtmlFromFile
	 * @param      {String}  relativePathFromAppFolder From the containing app folder, path to file
	 * @param      {String}  whereToAppend     Node who's innerHTML will be set to content
	 * @param      {String}  callback     What function to call after getting the file
	 */
	loadHtmlFromFile: function(relativePathFromAppFolder, whereToAppend, callback) {
		var _this = this;
		readFile(relativePathFromAppFolder, function(err, data) {
			_this.loadIntoAppendLocation(whereToAppend, data);
			callback();
		}, 'TEXT');
	},

	/**
	 * Called after xhr gets html content
	 * Main thing to note is that id fields are altered to be prefixed with SAGE2 assigned id
	 *
	 * @method     loadIntoAppendLocation
	 * @param      {String}  whereToAppend Node who's innerHTML will be set to content
	 * @param      {String}  responseText     Content of the file
	 */
	loadIntoAppendLocation: function(whereToAppend, responseText) {
		var content = "";
		// id and spaces because people aren't always consistent
		var idIndex;

		// find location of first id div. Because there will potentially be multiple apps.
		idIndex = this.findNextIdInHtml(responseText);

		// for each id, prefix it with this.id
		while (idIndex !== -1) {
			// based on id location move it over
			content += responseText.substring(0, idIndex);
			responseText = responseText.substring(idIndex);
			// collect up to the first double quote. design.html has double quotes, but HTML doesn't require.
			content += responseText.substring(0, responseText.indexOf('"') + 1);
			responseText = responseText.substring(responseText.indexOf('"') + 1);
			// apply id prefix
			content += this.id;
			// collect rest of id
			content += responseText.substring(0, responseText.indexOf('"') + 1);
			responseText = responseText.substring(responseText.indexOf('"') + 1);

			// find location of first id div. Because there will potentially be multiple apps.
			idIndex = this.findNextIdInHtml(responseText);
		}
		content += responseText;
		whereToAppend.innerHTML = content;
	},

	/**
	 * This returns the index of the first location of id
	 * Accounts for 0 to 3 spaces between id and =
	 *
	 * @method     findNextIdInHtml
	 */
	findNextIdInHtml: function(responseText) {
		// find location of first id div. Because there will potentially be multiple apps.
		// the multiple checks are incase writers are not consistent
		var idIndex = responseText.indexOf("id=");
		var ids1 = responseText.indexOf("id =");
		var ids2 = responseText.indexOf("id  =");
		var ids3 = responseText.indexOf("id   =");
		// if (idIndex isn't found) or (is found but ids1 also found and smaller than idIndex)
		if ((idIndex === -1) || (ids1 > -1 && ids1 < idIndex)) {
			idIndex = ids1;
		}
		if ((idIndex === -1) || (ids2 > -1 && ids2 < idIndex)) {
			idIndex = ids2;
		}
		if ((idIndex === -1) || (ids3 > -1 && ids3 < idIndex)) {
			idIndex = ids3;
		}
		return idIndex;
	},

});
