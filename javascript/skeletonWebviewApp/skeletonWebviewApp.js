//
// SAGE2 application: skeletonWebviewApp
// by: Dylan Kobayashi <dylank@hawaii.edu>
//
// Copyright (c) 2018
//

"use strict";

/*
How to use:


*/


var skeletonWebviewApp = sage2_webview_appCoreV01_extendWebview({
	webpageAppSettings: {
		setSageAppBackgroundColor: true, // Web pages without background values will be transparent.
		backgroundColor: "white", // Used if above is true, can also use rgb and hex strings
		enableRightClickNewWindow: false, // right clicking on images or links open new webview
		printConsoleOutputFromPage: true, // If true, when page prints to command line, app will print in display client

		// If you want your entries to appear before or after the default
		putAdditionalContextMenuEntriesBeforeDefaultEntries: true,
		// The following will include the default Webview context menu entry if set to true.
		enableUiContextMenuEntries: {
			navigateBack:       false, // alt left-arrow
			navigateForward:    false, // alt right-arrow
			reload:             true, // alt r
			autoRefresh:        false, // must be selected from UI context menu
			consoleViewToggle:  false, // must be selected from UI context menu
			zoomIn:             true, // alt up-arrow
			zoomOut:            true, // alt down-arrow
			urlTyping:          false, // must be typed from UI context menu
			copyUrlToClipboard: false, // must be typed from UI context menu
		},
	},
	init: function(data) {
		// Will be called after initial SAGE2 init()
		// this.element will refer to the webview tag
		this.resizeEvents = "continuous"; // Recommended not to change. Options: never, continuous, onfinish

		// Path / URL of the page you want to show
		this.changeURL(this.resrcPath + "/webpage/index.html", false);
	},
	load: function(date) {
		// OPTIONAL
		// The state will be automatically passed to your webpage through the handler you gave to SAGE2_AppState
		// Use this if you want to alter the state BEFORE it is passed to your webpage. Access with this.state
	},
	draw: function(date) {
		// OPTIONAL
		// Your webpage will be in charge of its view
		// Use this if you want to so something within the SAGE2 Display variables
		// Be sure to set 'this.maxFPS' within init() if this is desired.
		// FPS only works if instructions sets animation true
	},
	resize: function() {
		// OPTIONAL
	},
	getContextEntries: function() {
		// OPTIONAL
		// This can be used to allow UI interaction to your webpage
		// Entires are added after entries of enableUiContextMenuEntries 
		var entries = [];
		// entries.push({
		// 	description: "This text is seen in the UI",
		// 	callback: "makeAFunctionMatchingThisString", // The string will specify which function to activate
		// 	parameters: {},
		// 	// The called function will be passed an Object.
		// 	// Each of the parameter properties will be in that object
		// 	// Some properties are reserved and may be automatically filled in.
		// });
		entries.push({
			description: "Set Counter value",
			callback: "setCounterValueInPage", // function defined below
			inputField: true, // Takes typed input from UI
			inputFieldSize: 5, // How big to make the field
			parameters: {},
		});
		entries.push({
			description: "Force state update",
			callback: "giveContainerStateToWebpage", // function defined below
			parameters: {},
		});
		return entries;
	},

	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// Add optional functions

	setCounterValueInPage: function(responseObject) { // Handles the UI context menu entry
		this.callFunctionInWebpage("setCounterValue", responseObject.clientInput);
	},

	giveContainerStateToWebpage: function() {
		this.sendStateToWebpage();
	},

	printPropertyLine1: function(value) {
		console.log(value.line1);
	},

	printPropertyLine2: function(value) {
		console.log(value.line2);
	},

});