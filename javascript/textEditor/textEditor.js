// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2018

"use strict";

var textEditor = SAGE2_App.extend({
	init: function(data) {
		// SAGE2 vars
		this.SAGE2Init("div", data);
		this.element.id = "div" + data.id;
		this.resizeEvents = "continuous";

		// Sizing variables
		this.startingAppWidth = 600; // instruction file
		this.startingFontSize = ui.titleTextSize;

		// other
		this.arrayOfEditors = [];
		this.displayedTitle = "Text Editor";

		// Wait for ace library to load
		this.waitForAceLibraryToLoad();
	},

	// Wait for Ace lib to load
	waitForAceLibraryToLoad: function() {
		if (!ace) {
			window.requestAnimationFrame(()=>{
				this.waitForAceLibraryToLoad();
			});
		} else { // it was loaded
			this.setupEditor();
			this.loadFileIfGiven();
		}
	},

	// Ace lib was loaded, setup editor now.
	setupEditor: function() {
		// Add editor div to element
		this.editorDiv = document.createElement("div");
		this.editorDiv.id = this.element.id + "_Editor";
		this.editorDiv.style.position = "absolute";
		this.editorDiv.style.top = "0px";
		this.editorDiv.style.right = "0px";
		this.editorDiv.style.bottom = "0px";
		this.editorDiv.style.left = "0px";
		this.element.appendChild(this.editorDiv);

		// Create editor
		this.editor = ace.edit(this.editorDiv.id);
		// Starting text for editor (didn't implement file load yet)
		var startingText =
`
function helloWorld () {
	console.log("Hello World!");
}
`;
		this.session = new ace.EditSession(startingText);
		this.editor.setSession(this.session);
		this.editor.setTheme("ace/theme/monokai");
		this.editor.session.setMode("ace/mode/javascript");

		this.editor.setFontSize(this.startingFontSize);
	},

	loadFileIfGiven: function() {
		let _this = this;
		if (this.state.file) {
			// Read file from location, on result
			readFile(this.state.file, function(err, text) {
				if (!err) {
					// Set text
					_this.editor.setValue(text);
					_this.editor.clearSelection();
					_this.displayedTitle = _this.reducePathToFileName(_this.state.file);
					_this.updateTitle(_this.displayedTitle);
					// need to wait until next draw cycle before it can correctly resize itself.
					window.requestAnimationFrame((()=>{
						_this.editor.resize();
					}));
				}
			}, "TEXT"); // type is text
		}
	},

	reducePathToFileName: function(path) {
		while(path.indexOf("/") !== -1) {
			path = path.substring(path.indexOf("/") + 1);
		}
		while(path.indexOf("\\") !== -1) {
			path = path.substring(path.indexOf("\\") + 1);
		}
		return path;
	},

	load: function(date) {
	},

	draw: function(date) {
	},

	resize: function(date) {
		if (this.editor) {
			this.editor.resize();
		}
	},

	// ------------------------------------------------------------------------------------------------------------------

	/**
	 * Adds a clientId as an editer. Activates after launch or context menu edit.
	 * Everyone in the array should be able to update this app correctly and receive each other's updates.
	 *
	 * @method addClientIdAsEditor
	 * @param {Object} responseObject - Should contain the following.
	 * @param {Object} responseObject.clientId - Unique id of client given by server.
	 * @param {Object} responseObject.clientName - User input name of pointer.
	*/
	addClientIdAsEditor: function(responseObject) {
		// prevent multiple sends if there are more than 1 display.
		if (isMaster) {
			// add the client who responded to the list of editors.
			this.arrayOfEditors.push(responseObject.clientId);

			// send back to client the OK to start editing.
			var dataForClient = {};
			dataForClient.currentText = this.editor.getValue();
			dataForClient.title = this.displayedTitle;
			// sendDataToClient: function(clientDest, func, paramObj) appId is automatically added to param object
			this.sendDataToClient(responseObject.clientId, "setTextInEditor", dataForClient);
		}
	},

	handleTextUpdateFromClient: function(responseObject) {
		// Set text
		this.editor.setValue(responseObject.editorText);
		this.editor.clearSelection();
		this.editor.resize();

		// If master display, tell all clients to update
		if (isMaster) {
			var dataForClient = {};
			dataForClient.currentText = this.editor.getValue();
			for (var i = 0; i < this.arrayOfEditors.length; i++) {
				if (this.arrayOfEditors[i] == responseObject.clientId) {
					continue; // Don't send to source of update
				}
				dataForClient.clientDest = this.arrayOfEditors[i];
				// clientDest, function, param object for function. appId is automatically added.
				this.sendDataToClient(this.arrayOfEditors[i], "setTextInEditor", dataForClient);
			}
		}
	},

	handleTextDeltaFromClient: function(responseObject) {
		// Set text
		let delta = JSON.parse(responseObject.delta);
		this.editor.getSession().getDocument().applyDeltas([delta]); // NEEDS array
		this.editor.resize();
		// Put change into view
		this.editor.scrollToLine(delta.start.row - 1, true, true);

		// If master display, tell all clients to update
		if (isMaster) {
			// Prep data for sending
			var dataForClient = {};
			dataForClient.delta = responseObject.delta; // String format

			// Don't send to source of delta
			for (var i = 0; i < this.arrayOfEditors.length; i++) {
				if (this.arrayOfEditors[i] == responseObject.clientId) {
					continue; // Don't send to source of update
				}
				dataForClient.clientDest = this.arrayOfEditors[i];
				// clientDest, function, param object for function. appId is automatically added.
				this.sendDataToClient(this.arrayOfEditors[i], "editorTextDelta", dataForClient);
			}
		}
	},

	// ------------------------------------------------------------------------------------------------------------------

	getContextEntries: function() {
		var entries = [];
		var entry;

		// Create edit link for UI
		entry = {};
		// Has to be ahref to allow for backwards compatibility with right click menus
		entry.description = "Open edit page in new tab (proto)";
		entry.callback = "SAGE2_openPage";
		entry.parameters = {
			url: this.resrcPath + "webpage/index.html?appId=" + this.id + "&pointerName=guest&pointerColor=gray"
		};
		entries.push(entry);

		entry = {};
		// Has to be ahref to allow for backwards compatibility with right click menus
		entry.description = "Open as standalone page";
		entry.callback = "SAGE2_standAloneApp";
		entry.parameters = {
			url: this.resrcPath + "webpage/index.html?appId=" + this.id + "&pointerName=guest&pointerColor=gray"
		};
		entries.push(entry);
		
		entry = {};
		entry.description = "Copy edit URL to clipboard (use this if above doesnt work)";
		entry.callback = "SAGE2_copyURL";
		entry.parameters = {
			url: this.resrcPath + "webpage/index.html?appId=" + this.id + "&pointerName=guest&pointerColor=gray"
		};
		entries.push(entry);

		entries.push({description: "separator"});

		entry = {};
		entry.description = "Increase Font Size";
		entry.accelerator = "Alt \u2191";     // ALT up-arrow
		entry.callback = "changeFontSize";
		entry.parameters = {};
		entry.parameters.type = "increase";
		entries.push(entry);

		entry = {};
		entry.description = "Decrease Font Size";
		entry.accelerator = "Alt \u2193";     // ALT down-arrow
		entry.callback = "changeFontSize";
		entry.parameters = {};
		entry.parameters.type = "decrease";
		entries.push(entry);

		return entries;
	},

	event: function(eventType, position, user_id, data, date) {
		// arrow down
		if (data.code === 40 && data.state === "down") {
			if (data.status.ALT) {
				// ALT-down_arrow zooms out
				this.changeFontSize({type: "decrease"});
			}
		}
		// arrow up
		if (data.code === 38 && data.state === "down") {
			if (data.status.ALT) {
				// ALT-up_arrow zooms in
				this.changeFontSize({type: "increase"});
			}
		}
	},

	// ------------------------------------------------------------------------------------------------------------------

	changeFontSize: function(responseObject) {
		if (responseObject.type === "increase") {
			this.startingFontSize++;
			this.editor.setFontSize(this.startingFontSize);
		} else if (responseObject.type === "decrease") {
			this.startingFontSize--;
			if (this.startingFontSize <= 0) {
				this.startingFontSize = 1;
			}
			this.editor.setFontSize(this.startingFontSize);
		}
	},
});
