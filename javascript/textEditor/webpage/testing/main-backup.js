// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-17

"use strict";

/* global  SAGE2Connection*/

/*
File has parts:
	1. Any actions needed to run before connecting to server.
	2. Connect to server
	3. Functions that the app expects to call
	4. Functions for usage in the page

*/

/* ------------------------------------------------------------------------------------------------------------------
// 1
// ------------------------------------------------------------------------------------------------------------------
Finalize the page
*/


var lastEditorText = null;
var editor = ace.edit("editor");
var startingText =
`
Loading...
`;
var session = new ace.EditSession(startingText);
editor.setSession(session);
editor.setTheme("ace/theme/monokai");
editor.session.setMode("ace/mode/javascript");

console.log(editor.getValue());


setInterval(checkIfNeedToSendServerText, 10);




/* ------------------------------------------------------------------------------------------------------------------
// 2
// ------------------------------------------------------------------------------------------------------------------
Connect to the server
*/

// first describe any reaction after connecting
SAGE2Connection.afterSAGE2Connection = addThisClientAsEditor;

// This this is part of app code, it will use the current window values to connect.
// But if the page was hosted elsewhere, parameters would be required.
SAGE2Connection.initS2Connection();

/* ------------------------------------------------------------------------------------------------------------------
// 3
// ------------------------------------------------------------------------------------------------------------------
The following functions are for communication handling.
*/

// After establishing connection, call this function to let app know this page is acting as editor.
function addThisClientAsEditor() {
	// callFunctionOnApp: function(functionName, parameterObject) { // autofilled id by server
	SAGE2Connection.callFunctionOnApp("addClientIdAsEditor", {});
}


// Has the initial state on connection
// HAVE to use this name for compliance with UI version
function setTextInEditor(data) {
	let textPrefix = getWordsBeforeCursor(3);
	let textSuffix = getWordsAfterCursor(3);
	editor.setValue(data.currentText);
	lastEditorText = data.currentText;
	editor.clearSelection();
}
function getWordsBeforeCursor(amountOfWords) {
	let startingPos = editor.getCursorPosition();
	for (let i  = 0; i < amountOfWords; i++) {
		editor.selection.selectWordLeft()
	}
	let wordsBefore = editor.getSelectedText();
	editor.selection.setSelectionRange({start:startingPos, end:startingPos});
	return wordsBefore;
}
function getWordsAfterCursor(amountOfWords) {
	let startingPos = editor.getCursorPosition();
	for (let i  = 0; i < amountOfWords; i++) {
		editor.selection.selectWordRight()
	}
	let wordsBefore = editor.getSelectedText();
	editor.selection.setSelectionRange({start:startingPos, end:startingPos});
	return wordsBefore;
}

// To server
function sendServerThisEditorText() {
	let currentEditorText = editor.getValue();
	lastEditorText = currentEditorText;
	// Func name, param object
	SAGE2Connection.callFunctionOnApp("handleTextUpdateFromClient", {editorText: currentEditorText});
}




/* ------------------------------------------------------------------------------------------------------------------
// 4
// ------------------------------------------------------------------------------------------------------------------
Functions for usage in the page
*/

function checkIfNeedToSendServerText() {
	if (lastEditorText == null) {
		return;
	} else if (lastEditorText != editor.getValue()) {
		console.log("Sending diff");
		sendServerThisEditorText();
	}
}



