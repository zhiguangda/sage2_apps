// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

"use strict";

var vtiViewer = SAGE2_App.extend({
	init: function(data) {
		this.SAGE2Init("div", data);

		this.resizeEvents = "onfinish";
		this.moveEvents   = "onfinish";

		this.appSpecific();
	},

	appSpecific: function() {
		setTimeout(() => {
			this.delayedFileRead();
		}, 1000);
	},

	delayedFileRead: function() {
		this.element.style.background = "white";
		// If a file was given, there should be a location.
		let location = this.state.file;
		let usingExample = false;
		if (!location) {
			// No location, mean use a default included file
			location = "https://data.kitware.com/api/v1/item/59e12e988d777f31ac6455c5/download";
			this.fullFileLocation = location;
		} else {
			this.fullFileLocation = "https://" + ui.json_cfg.host + location;
		}
		this.element.innerHTML += "<h1>Loading...</h1>";
		this.element.innerHTML += "<h1>" + this.fullFileLocation + "</h1>";

		this.launchWebsites();
	},

	launchWebsites: function(){
		this.websites = {};
		// The file load will be added later
		this.websites.kitware = this.getWebsiteTracker("https://kitware.github.io/vtk-js/examples/VolumeViewer/index.html");
		
		// Start them.
		this.kitwareProcessing();
	},

	getWebsiteTracker: function(url){
		return {
			url,
			phase: "openPage",
			webview: null
		};
	},

	findChildWebviewApp: function(urlRequirement) {
		let cai = this.childrenAppIds;
		let app;
		for (let i = 0; i < cai.length; i++) {
			app = applications[this.childrenAppIds[i]];
			if (app.state
				&& app.state.url
				&& (app.state.url.includes(urlRequirement))) {
				return app;
			}
		}
		return null;
	},

	load: function(date) {
	},

	draw: function(date) {
	},

	move: function(date) {
	},

	startResize: function(date) {
	},

	resize: function(date) {
	},

	event: function(eventType, position, user, data, date) {
	}
});


// Add functions from external files
s2_vtiViewer_delayedFunctionAdd_kitwareSupport();