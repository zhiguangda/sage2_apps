//
// SAGE2 application: VTK viewer
// by: Dylan Kobayashi <dylank@hawaii.edu>
//
// Copyright (c) 2018
//

"use strict";

/* global  require */

var vtkVolumeViewer = SAGE2_App.extend({
	init: function(data) {
		if (this.isElectron()) {
			// Create div into the DOM
			this.SAGE2Init("webview", data);
			// Create a layer for the console
			this.createLayer("rgba(0,0,0,0.85)");
			// clip the overflow
			this.layer.style.overflow = "hidden";
			// create a text box
			this.pre = document.createElement('pre');
			// allow text to wrap inside the box
			this.pre.style.whiteSpace = "pre-wrap";
			// Add it to the layer
			this.layer.appendChild(this.pre);
			this.console = false;
			// Add certificate error handler, it needs electron reference
			this.handleCertificateError();
		} else {
			// Create div into the DOM
			this.SAGE2Init("div", data);
			this.element.innerHTML = "<h1>Webview only supported using Electron as a display client</h1>";
		}
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'white';

		// move and resize callbacks
		this.resizeEvents = "continuous";
		this.modifiers    = [];

		// Content type: web, youtube, ..
		this.contentType = "web";
		// Muted audio or not, only on isMaster node
		this.isMuted = false;
		// Is the page loading
		this.isLoading = false;

		// not sure
		this.element.style.display = "inline-flex";

		// Webview settings
		this.element.autosize  = "on";
		this.element.plugins   = "on";
		this.element.allowpopups = false;
		this.element.allowfullscreen = false;
		// turn off nodejs intergration for now
		this.element.nodeintegration = 0;
		// disable fullscreen
		this.element.fullscreenable = false;
		this.element.fullscreen = false;
		// add the preload clause
		this.addPreloadFile();
		// security or not: this seems to be an issue often on Windows
		this.element.disablewebsecurity = false;

		// Set a session per webview, so not zoom sharing per origin
		this.element.partition = data.id;

		// initial size
		this.element.minwidth  = data.width;
		this.element.minheight = data.height;

		// Default title
		this.title = "Webview";

		// Get the URL from parameter or session
		var view_url = data.params || this.state.file || this.state.url;
		var video_id, ampersandPosition;

		// Is the page hosted by SAGE server
		this.connectingToSageHostedFile = true;
		// Store the zoom level, when in desktop emulation
		this.zoomFactor = 1;
		// Auto-refresh time
		this.autoRefresh = null;

		var _this = this;

		// Console message from the embedded page
		this.element.addEventListener('console-message', function(event) {
			// console.log('Webview>	console:', event.message); // Disabling display prints for now
			// Add the message to the console layer
			_this.pre.innerHTML += 'Webview> ' + event.message + '\n';
			try {
				let obj = JSON.parse(event.message);
				if (obj.s2) {
					if (obj.s2 === "status"){
						if (obj.nameOfValue === "title") {
							_this.messageUpdateTitle(obj.value);
						}
					}
					if (obj.s2 === "state"){
						_this.messageUpdateStateValue(obj.nameOfValue, obj.value, obj.propagateChanges)
					}
				}
			} catch(e) {
				// console.log(e);
			 } // Ignore
		});

		// Adds the session cookie to the Webview's cookies
		if (this.connectingToSageHostedFile && getCookie("session")) {
			var webview = this.element;
			// Wait until the dom is ready before add the cookie
			webview.addEventListener("dom-ready", function() {
				// webview.openDevTools(); // Debugging
				// Parse out the original URL from the session URL
				var webviewContents = webview.getWebContents();
				var urlWithSession = webviewContents.getURL();
				// Check if URL has session.html before parsing and reloading page without it
				if (urlWithSession.indexOf("session.html") > 0) {
					var urlRoot = urlWithSession.substring(0, urlWithSession.indexOf("session.html") - 1);
					var url = urlRoot + urlWithSession.substring(urlWithSession.indexOf("page") + 5, urlWithSession.length);
					// Add the session to the Webview's cookies
					webviewContents.session.cookies.set(
						{
							url: urlRoot,
							name: "session",
							value: getCookie("session")
						},
						function() {
						}
					);
					// Reloads the Webview without the session URL
					_this.changeURL(url, false);
				}
			});
		}

		// ------------------------------------------------------------------------------------------------------------------------
		// Check file location
		this.vtk = {};
		let fileLocation = this.state.file;
		this.vtk.fileLocation = fileLocation;
		if (!fileLocation) {
			// No fileLocation, mean use a default included file
			//?fileURL=https://data.kitware.com/api/v1/item/59e12e988d777f31ac6455c5/download
			fileLocation = "https://data.kitware.com/api/v1/item/59e12e988d777f31ac6455c5/download";
			this.vtk.fullFileLocation = fileLocation;
			this.vtk.fileLocation = "Visible Human";
		} else {
			this.vtk.fullFileLocation = "https://" + ui.json_cfg.host + location;
		}
		// Make the spinner
		let newTitle = ' <i class="fa fa-spinner fa-spin"></i> Loading ' + fileLocation;
		// call the base class method
		this.updateTitle(newTitle);
		view_url = this.resrcPath + "/webpage/index.html?fileURL=" + fileLocation;

		// Set the URL and starts loading
		this.changeURL(view_url, false);
	},

	/**
	 * Handles certificate errors from loading webpages.
	 *
	 * @method     handleCertificateError
	 */
	handleCertificateError: function() {
		if (this.addedHandlerForCertificteError) {
			return;
		}
		var content = this.element.getWebContents();
		var _this = this;
		if (!content) {
			window.requestAnimationFrame(function() {
				_this.handleCertificateError();
			});
		} else {
			// content.on('certificate-error', function(event) {
			// 	console.log('Webview>	certificate error:', event);
			// 	_this.pre.innerHTML += 'Webview>	certificate error:' + event + '\n';
			// 	_this.element.executeJavaScript(
			// 		"document.body.innerHTML = '<h1>This webpage has invalid certificates and cannot be loaded</h1>'");
			// });
			content.on('certificate-error', function(event, url, error, certificate, callback) {
				// This doesnt seem like a security risk yet
				if (error === "net::ERR_CERTIFICATE_TRANSPARENCY_REQUIRED") {
					console.log('Webview>certificate error1:', url, error, certificate);
					// we ignore the certificate error
					event.preventDefault();
					callback(true);
				} else {
					// More troubling error
					console.log('Webview>certificate error2:', url, error, certificate);
					// Add the message to the console layer
					_this.pre.innerHTML += 'Webview>certificate error:' + event + '\n';
					_this.element.executeJavaScript(
						"document.body.innerHTML = '<h1>This webpage has invalid certificates and cannot be loaded</h1>'");
					// Denied
					callback(false);
				}
			});
			this.addedHandlerForCertificteError = true;
		}
	},

	/**
	 * Determines if electron is the renderer (instead of a browser)
	 *
	 * @method     isElectron
	 * @return     {Boolean}  True if electron, False otherwise.
	 */
	isElectron: function() {
		return (typeof window !== 'undefined' && window.process && window.process.type === "renderer");
	},

	/**
	 * Loads the components to do a file preload on a webpage.
	 * Needs to be within an Electron browser to work.
	 *
	 * @method     addPreloadFile
	 */
	addPreloadFile: function() {
		if (!this.isElectron()) {
			return; // return if not electron.
		}
		// if it's not running inside Electron, do not bother
		if (!this.isElectron) {
			return;
		}
		// load the nodejs path module
		var path = require("path");
		// access the remote electron process
		var app = require("electron").remote.app;
		// get the application path
		var appPath = app.getAppPath();
		// // split the path at node_modules
		// var subPath = appPath.split("node_modules");
		// // take the first element which contains the current folder of the application
		// var rootPath = subPath[0];
		// // add the relative path to the webview folder
		var rootPath = appPath;
		var preloadPath = path.join(rootPath, 'public/uploads/apps/Webview', 'SAGE2_script_supplement.js');
		// finally make it a local URL and pass it to the webview element
		this.element.preload = "file://" + preloadPath;
	},

	// Sync event when shared
	load: function(date) {
		// Will send full state object to the webpage
		this.element.executeJavaScript(
			"SAGE2_AppState.fullStateHandler("
			+ JSON.stringify(this.state)
			+ ");"
		);
	},

	draw: function(date) {
	},

	changeURL: function(newlocation, remoteSync) {
		// trigger the change
		this.element.src = newlocation;
		this.SAGE2Sync(remoteSync); // remote sync
	},

	isHostedBySelf: function(newlocation) {
		// Combine the hostnames/IPs listed in the configuration file
		var allHostNames = [].concat(ui.json_cfg.alternate_hosts, ui.json_cfg.host);
		// Check if newlocation has any of the hostnames
		for (let i = 0; i < allHostNames.length; i++) {
			if (allHostNames[i].trim().length > 1) {
				if (newlocation.includes(allHostNames[i])) {
					return true;
				}
			}
		}
		return false;
	},

	resize: function(date) {
		// Called when window is resized
		this.element.style.width  = this.sage2_width  + "px";
		this.element.style.height = this.sage2_height + "px";

		// resize the console layer
		if (this.layer) {
			// make sure the layer exist first
			this.layer.style.width  = this.element.style.width;
			this.layer.style.height = this.element.style.height;
		}
		this.refresh(date);
	},

	quit: function() {
		if (this.autoRefresh) {
			// cancel the autoreload timer
			clearInterval(this.autoRefresh);
		}
	},

	getContextEntries: function() {
		var entries = [];
		var entry;

		entry = {};
		entry.description = "Show/Hide Console";
		entry.callback = "showConsole";
		entry.parameters = {};
		entries.push(entry);

		return entries;
	},

	zoomPage: function(responseObject) {
		if (this.isElectron()) {
			var dir = responseObject.dir;

			// zoomin
			if (dir === "zoomin") {
				this.state.zoom *= 1.50;
				this.element.setZoomFactor(this.state.zoom);
			}

			// zoomout
			if (dir === "zoomout") {
				this.state.zoom /= 1.50;
				this.element.setZoomFactor(this.state.zoom);
			}

			this.refresh();
		}
	},

	showConsole: function(responseObject) {
		if (this.isElectron()) {
			if (this.console) {
				this.hideLayer();
				this.console = false;
			} else {
				this.showLayer();
				this.console = true;
			}
		}
	},

	event: function(eventType, position, user_id, data, date) {
		if (this.isElectron()) {
			// Making Integer values, seems to be required by sendInputEvent
			var x = Math.round(position.x);
			var y = Math.round(position.y);
			var _this = this;

			if (eventType === "pointerPress") {
				// click
				this.element.sendInputEvent({
					type: "mouseDown",
					x: x, y: y,
					button: data.button,
					modifiers: this.modifiers,
					clickCount: 1
				});
			} else if (eventType === "pointerMove") {
				// move
				this.element.sendInputEvent({
					type: "mouseMove",
					modifiers: this.modifiers,
					x: x, y: y
				});
			} else if (eventType === "pointerRelease") {
				// click release
				this.element.sendInputEvent({
					type: "mouseUp",
					x: x, y: y,
					button: data.button,
					modifiers: this.modifiers,
					clickCount: 1
				});
			} else if (eventType === "pointerScroll") {
				// Scroll events: reverse the amount to get correct direction
				this.element.sendInputEvent({
					type: "mouseWheel",
					deltaX: 0, deltaY: -1 * data.wheelDelta,
					x: x, y: y,
					modifiers: this.modifiers,
					canScroll: true
				});
			} else if (eventType === "widgetEvent") {
				// widget events
			} else if (eventType === "keyboard") {
				// TODO double check this effect on multiple displays
				if (this.contentType !== "unity") {
					this.element.focus();
				}

				// send the character event
				this.element.sendInputEvent({
					// type: "keyDown",
					// Not sure why we need 'char' but it works ! -- Luc
					type: "char",
					keyCode: data.character
				});
				setTimeout(function() {
					_this.element.sendInputEvent({
						type: "keyUp",
						keyCode: data.character
					});
				}, 0);
			} else if (eventType === "specialKey") {
				// clear the array
				this.modifiers = [];
				// store the modifiers values
				if (data.status && data.status.SHIFT) {
					this.modifiers.push("shift");
				}
				if (data.status && data.status.CTRL) {
					this.modifiers.push("control");
				}
				if (data.status && data.status.ALT) {
					this.modifiers.push("alt");
				}
				if (data.status && data.status.CMD) {
					this.modifiers.push("meta");
				}
				if (data.status && data.status.CAPS) {
					this.modifiers.push("capsLock");
				}

				// SHIFT key
				if (data.code === 16) {
					if (data.state === "down") {
						this.element.sendInputEvent({
							type: "keyDown",
							keyCode: "Shift"
						});
					} else {
						this.element.sendInputEvent({
							type: "keyUp",
							keyCode: "Shift"
						});
					}
				}
				// backspace key
				if (data.code === 8 || data.code === 46) {
					if (data.state === "down") {
						// The delete is too quick potentially.
						// Currently only allow on keyup have finer control
					} else {
						this.element.sendInputEvent({
							type: "keyUp",
							keyCode: "Backspace"
						});
					}
				}

				if (data.code === 37 && data.state === "down") {
					// arrow left
					// send the left arrow key
					this.element.sendInputEvent({
						type: "keyDown",
						keyCode: "Left",
						modifiers: null
					});
					this.refresh(date);
				} else if (data.code === 38 && data.state === "down") {
					// arrow up
					if (data.status.ALT) {
						// ALT-up_arrow zooms in
						this.zoomPage({dir: "zoomin"});
					} else {
						this.element.sendInputEvent({
							type: "keyDown",
							keyCode: "Up",
							modifiers: null
						});
					}
					this.refresh(date);
				} else if (data.code === 39 && data.state === "down") {
					// arrow right
					// send the right arrow key
					this.element.sendInputEvent({
						type: "keyDown",
						keyCode: "Right",
						modifiers: null
					});
					this.refresh(date);
				} else if (data.code === 40 && data.state === "down") {
					// arrow down
					if (data.status.ALT) {
						// ALT-down_arrow zooms out
						this.zoomPage({dir: "zoomout"});
					} else {
						this.element.sendInputEvent({
							type: "keyDown",
							keyCode: "Down",
							modifiers: null
						});
					}
					this.refresh(date);
				}
			}
		}
	},

	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------

	messageUpdateTitle: function(title) {
		this.updateTitle(title);
	},

	messageUpdateStateValue: function(nameOfValue, value, propagateChanges) {
		if (!this.state[nameOfValue]) {
			console.log("Warning: app trying to set a value that doens't exist in state: " + nameOfValue, this.state);
		}
		this.state[nameOfValue] = value;
		if (propagateChanges) {
			this.SAGE2Sync(true); // True means remote sync. false is send updates to only this server's clients
		}
	},

	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------

	vtkLoadedTitleUpdate: function() {
		let fname = this.vtk.fileLocation.split("/");
		fname = fname[fname.length - 1]; // Take the last one
		fname = this.vtk.fileLocation.split("\\");
		fname = fname[fname.length - 1]; // Take the last one
		fname = fname.split(".")[0];

		let newTitle = "VTK Volume Viewer - " + fname;
		// call the base class method
		this.updateTitle(newTitle);
	},

	vtkStateUpdateCamera: function(state) {
		// Preserver state values
		this.state.cameraPosition = state.position;
		this.state.cameraFocalPoint = state.focalPoint;
		this.state.cameraViewAngle = state.viewAngle;
		this.state.cameraViewUp = state.viewUp;

		// If there has been camera change, update state
		if (!this.isVtkCameraSamePosition()) {
			// Track the previous value
			this.vtk.previousCameraValue  = state.position;
			this.SAGE2Sync(true); // remote sync
			console.log("State update of camera:" + this.state.cameraPosition, state);
		}
	},

	isVtkCameraSamePosition: function(optionalPosition) {
		// First time run won't have value initialized
		if (!this.vtk.previousCameraValue) {
			this.vtk.previousCameraValue = [0, 0, 0];
		}
		// If optional value give, compare that against previous
		if (optionalPosition) {
			if (this.vtk.previousCameraValue === optionalPosition) {
				return true;
			} else {
				return false;
			}
		}
		// Otherwise compare against the state
		if (this.state.cameraPosition) {
			let cpos = this.state.cameraPosition;
			let ppos = this.vtk.previousCameraValue;
			if ((cpos[0] === ppos[0]) || (cpos[1] === ppos[1]) || (cpos[2] === ppos[2])){
				return true;
			}
		}
		return false;
	},

	vtkSetCameraPositionToState: function() {
		this.element.executeJavaScript(
			"vtkRefs.setCameraState("
			+ JSON.stringify(this.state.cameraPosition)
			+ ", " + JSON.stringify(this.state.cameraFocalPoint)
			+ ", " + JSON.stringify(this.state.cameraViewAngle)
			+ ", " + JSON.stringify(this.state.cameraViewUp)
			+ ");"
			+ "vtkRefs.redraw();"
		);
	},


});




/*


	sendAlertCode: function() {
		this.element.executeJavaScript(
			"alert('where is this and or does it work?')",
			false,
			function() {
				console.log("sendAlertCode callback initiated");
			}
		);
	},

	//Called after each page load, currentl prevents the lock from input focus.
	
	codeInject: function() {
		// Disabling text selection in page because it blocks the view sometimes
		// done by injecting some CSS code
		// Also disabling grab and drag events
		this.element.insertCSS(":not(input):not(textarea), " +
			":not(input):not(textarea)::after, " +
			":not(input):not(textarea)::before { " +
				"-webkit-user-select: none; " +
				"user-select: none; " +
				"cursor: default; " +
				"-webkit-user-drag: none;" +
				"-moz-user-drag: none;" +
				"user-drag: none;" +
			"} " +
			"input, button, textarea, :focus { " +
				"outline: none; " +
			"}");
	},



*/