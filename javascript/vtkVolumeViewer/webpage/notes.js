/*


Using the vtk instructions didn't lead to a working volume viewer.


Problem:
  mcss loaders aren't part of the dependencies
    Also couldn't find one that works
  also svg loader

  had to edit:
    node_modules/vtk.js/Sources/Interaction/UI/
                                              FPSMonitor
                                              VolumeController
    Make local variables to replace the mcss and svg loading
      The svg can be stacked in multi line strings using a single ` to start and end
      For example: ` this is the first line
                    No need for end of line escape like \
                    All spaces to the left are included
                    But it doesn't play nice with adding strings after
                    `
      For the mcss, most of them are assignment of classname
        Could make an object to store the reference of the name, then has same effect

  Before building
    Create references to necessary vtk pieces and add them to the window
    This allows later direct usage instead of having to build after each change

    Widget doesn't fully work yet
      Able to set value, but unable to get the update
      Needed function references to partially update

      Test code:
        var r =  vtkRefs.volumeControlWidget.get();
        var widget = r.widget;
        
        // Changes value example
        widget.getGaussians()[0].height = 1;



  After building:
    Change the .mcss to .css
    Include with distribution
    Add them at end of html
      Something in the scripts are overriding them
  Running can access window assigned variables




*/




