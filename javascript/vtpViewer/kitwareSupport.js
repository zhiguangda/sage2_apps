
function s2_vtpViewer_delayedFunctionAdd_kitwareSupport(obj) {

// Actual addition
vtpViewer.prototype.kitwareProcessing = function() {
	let site = this.websites.kitware;
	let swv = site.webview;
	switch(site.phase) {
		case "openPage":
			// First add the url for the file
			site.url += "?fileURL=" + this.fullFileLocation;
			// launchAppWithValues: function(appName, paramObj, x, y, funcToPassParams)
			this.launchAppWithValues(
				"Webview",
				{
					action: "address",
					clientInput: site.url
				},
				// offset
				this.sage2_x + 100,
				this.sage2_y + 100,
				"navigation");
			site.phase = "done";
			site.phaseDelay = 7000;
			break;
	// ----------------------------------------------------------------------------------------------------------------------
		default:
			site.phase = "done";
			break;
	};

	// If not done, then come back later
	if (site.phase != "done") {
		setTimeout(() => {
			this.kitwareProcessing(); // SPECIFIC TO HANDLER
		}, site.phaseDelay);
	}
};

} // s2_vtpViewer_delayedFunctionAdd_kitwareSupport