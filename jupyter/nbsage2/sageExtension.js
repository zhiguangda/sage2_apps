
var shortCutsKeys = [];
var shortCutsDictionary = [];
var wsio;
var connected;
var sageConnection;


//object that handle connection
function SAGE2Connection(url) {
    this.url = url;
    this.connected = true;
    this.trackedCell = [];
    this.wsio = new WebsocketIO(url);
    var _this = this
    this.wsio.open(function() {
        var clientDescription = {
            clientType: "jupyter",
            requests: {
                config: true,
                version: true,
                time: false,
                console: false
            },
        };
        _this.wsio.on('initialize', function(data) {
            _this.id = data.UID;
	    _this.startConnection();
        });
        _this.wsio.emit('addClient', clientDescription);
    });

}

//open a connection
SAGE2Connection.prototype.startConnection = function() {
    this.wsio.emit('startJupyterSharing', {id: this.id,
        title: "jupyter",
        color: "green",
        src: "raw", type: "image/jpeg", encoding: "binary",
        width: 600, height: 600});
}

//send updates to sage
SAGE2Connection.prototype.sendUpdate = function(data, cellId) {
    this.wsio.emit('updateJupyterSharing', {
        id: this.id,
        src: data,
        cellId: cellId});
}



function addDiv(id) {
    var div = createElement("div");
    div.setAttribute("id",id);
    document.body.appendChild(div);
    return div;
}

function createElement(type) {
    var element = document.createElement(type);
    return element;
}

function createTable(rows, columns, parent) {
    var table = createElement("table");
    var tableBody = createElement("tbody");
    var elements = [];
    table.appendChild(tableBody);
    for(var i = 0; i < rows; i++) {
        var row = createElement("tr");
        tableBody.appendChild(row);
        for(var j = 0; j < columns; j++) {
            var column = createElement("td");
	    if (j === 0) {
		column.setAttribute("width","25%");
	    }
            row.appendChild(column);
            elements.push(column);
        }
    }
    table.setAttribute("width","100%");
    parent.appendChild(table);
    return elements;
}

function addElementsToTable(elements, tableElements) {
    for(var i in tableElements) {
        elements[i].appendChild(tableElements[i]);
    }
}

function createDialog() {
    //create dialog structure
    var dialog = addDiv("settingsDialog");
    dialog.setAttribute("class", "dialog");
    document.body.appendChild(dialog);
    var divLogo = createElement("div");
    divLogo.setAttribute("id","dialogLogo");
    var divAddress = createElement("div");
    divAddress.setAttribute("id","dialogAddress");
    var divButtons = createElement("div");
    divButtons.setAttribute("id","dialogButtons");
    dialog.appendChild(divLogo);
    dialog.appendChild(divAddress);
    dialog.appendChild(divButtons);

    // set sage logo
    var logo = createElement("img");
    logo.setAttribute("src","/user/luc/nbextensions/nbsage2/img/sage2_logo.png");
    logo.setAttribute("id","dialogLogoImage");
    divLogo.appendChild(logo);

    // set div address
    var table = createTable(1,2,divAddress);
    var tableElements = [];
    var first = createElement("label");
    first.innerHTML = "SAGE2 server URL: ";
    var second = createElement("input");
    second.setAttribute("id","addressInput")
    second.setAttribute("type","text");
    second.setAttribute("width","100%");
    tableElements.push(first);
    tableElements.push(second);
    addElementsToTable(table, tableElements);

    // buttons
    var spanPush = createElement("span");
    var spanClose = createElement("close");
    spanPush.innerHTML = "Connect";
    spanClose.innerHTML = "Close";
    spanPush.setAttribute("class", "button");
    spanPush.setAttribute("id", "pushBtn");
    spanClose.setAttribute("class", "button");
    spanClose.setAttribute("id", "closeBtn");
    spanClose.addEventListener("click", hideDialog, false);
    spanPush.addEventListener("click", connectToSage, false);
    divButtons.appendChild(spanPush);
    divButtons.appendChild(spanClose);

}

function hideDialog() {
    $(document).ready(function() { 
            IPython.keyboard_manager.mode = "html";
            $("#overlay").hide();
            $("#settingsDialog").hide();
    });
}

// callback function for graphs interaction
function updateSageCanvas(element) {
    var elementTmp = element.target.parentElement;
    updateCanvasHelper(elementTmp);
}
function updateSageImage(element) {
    var elementTmp = element.target.parentElement;
    updateImageHelper(elementTmp);
}

// retrieve the new graph
function updateCanvasHelper(element) {
	var selectedCell = IPython.notebook.get_selected_cell();
	var elementTmp = element;
	if (sageConnection.trackedCell.indexOf(selectedCell) > -1) {
		while (elementTmp) {
			if(elementTmp.className === "cell code_cell rendered selected") {
				var canvas = element.parentElement.getElementsByClassName("bk-canvas")[0];
				if (canvas !== undefined) {
					var img = canvas.toDataURL("image/png", 1.0);
					if(img !== undefined && selectedCell !== undefined) {
						sageConnection.sendUpdate(img, selectedCell.cell_id);
						return;
					}
				} else {
					return;
				}
			}
			if(elementTmp.className === "cell code_cell unselected rendered") {
				return;
			}
			elementTmp = elementTmp.parentElement;
		}
	}
}
function updateImageHelper(element) {
	var selectedCell = IPython.notebook.get_selected_cell();
	var elementTmp = element;
	if (sageConnection.trackedCell.indexOf(selectedCell) > -1) {
		while (elementTmp) {
			if(elementTmp.className === "cell code_cell rendered selected") {
				var div = element.parentElement.getElementsByClassName("output_subarea")[0];
				if (div !== undefined) {
					var img = div.getElementsByTagName("img")[0];
					if(img !== undefined && selectedCell !== undefined) {
						sageConnection.sendUpdate(img.src, selectedCell.cell_id);
						return;
					}
				} else {
					return;
				}
			}
			if(elementTmp.className === "cell code_cell unselected rendered") {
				return;
			}
			elementTmp = elementTmp.parentElement;
		}
	}
}


// push the image to sage, callback function of the push button
function pushToSage() {
	if (sageConnection.connected) {
		var selectedCell = IPython.notebook.get_selected_cell();
		if (sageConnection.trackedCell.indexOf(selectedCell) <= -1) {
			sageConnection.trackedCell.push(selectedCell);
			var a = document.getElementsByClassName("output_wrapper");
			var elements = document.getElementsByClassName("bk-canvas");
			if (elements.length > 0) {
				// Bokeh
				for (var i = 0; i < a.length; i++) {
					a[i].addEventListener('mouseup',    updateSageCanvas, true);
					a[i].addEventListener('mousewheel', updateSageCanvas, true);
				}
				for (var i in elements) {
					updateCanvasHelper(elements[i]);
				}
			} else {
				// matplotlib: output_subarea output_png
				for (var i = 0; i < a.length; i++) {
					a[i].addEventListener('mouseup',    updateSageImage, true);
					a[i].addEventListener('mousewheel', updateSageImage, true);
				}
				elements = document.getElementsByClassName("output_png");
				for (var i in elements) {
					updateImageHelper(elements[i]);
				}
			}
		}
	}
}

// callback function of the connect button (cloud icon)
function connectToSage() {
	var url = document.getElementById("addressInput").value;
	if (url.search("https") > -1) {
		url = url.replace("https", "wss");
	} else if (url.search("http") > -1) {
		url = url.replace("http", "ws");
	} else if (!url.startsWith("ws")) {
		// otherwise, add wss
		url = "wss://" + url;
	}
	sageConnection = new SAGE2Connection(url);
	hideDialog();
}


define([
    'jquery'
],function() {
    "use strict";
    function _on_load(){
        for(var i in IPython.keyboard_manager.command_shortcuts._shortcuts) {
            shortCutsKeys.push(i);
            shortCutsDictionary[i] = IPython.keyboard_manager.command_shortcuts._shortcuts[i];
        }
        addDiv("overlay");
        createDialog();

        IPython.toolbar.add_buttons_group([
            {
                id : 'sage_connect',
                label : 'Connect to sage server',
                icon : 'fa-cloud',
                callback : function () {
                    process_solution();
                }
            }
         ]);  

        IPython.toolbar.add_buttons_group([
            {
                id : 'push_image',
                label : 'Push image to sage server',
                icon : 'fa-upload',
                callback : function () {
                    pushToSage();
                }
            }
        ]);  

        var load_css = function (name) {
            var link = document.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = require.toUrl(name);
            document.getElementsByTagName("head")[0].appendChild(link);
        };

        var load_js = function (name) {
            var link = document.createElement("script");
            link.setAttribute("src", name);
            document.getElementsByTagName("head")[0].appendChild(link);
        }

	load_css('/user/luc/nbextensions/nbsage2/styles/sageExtension.css');
	load_js( '/user/luc/nbextensions/nbsage2/script/websocket.io.js');

    }


    function process_solution() {
        $(document).ready(function() { 
            var selectedCell = IPython.notebook.get_selected_cell();
            IPython.keyboard_manager.mode = "html";
            $("#overlay").show();
            $("#settingsDialog").show();
        }); 
    }



    return {load_ipython_extension: _on_load };

})
